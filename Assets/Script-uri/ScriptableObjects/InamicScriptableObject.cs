using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu]
public class InamicScriptableObject: ScriptableObject
{
    public float puncteViata;
    public float daune;
    public float distanta;
    public float vitezaMiscare;
}
