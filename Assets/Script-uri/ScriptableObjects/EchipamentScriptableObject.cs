using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu]
public class EchipamentScriptableObject : ScriptableObject
{

    [SerializeField] 
    bool pentruArmament;
    public bool PentruArmament { get => pentruArmament; private set => pentruArmament = value; }

    [SerializeField] 
    bool specificArmament;
    public bool SpecificArmament { get => specificArmament; private set => specificArmament = value; }

    [SerializeField]
    private string nume;

    public string Nume { get => nume; private set => nume = value; }

    [SerializeField]
    [TextArea]
    private string descriere;

    public string Descriere { get => descriere; private set => descriere = value; }

    [SerializeField]
    private List<float> multiplicatoare;

    public List<float> Multiplicatoare { get => multiplicatoare; private set => multiplicatoare = value; }

    [SerializeField]
    private int nivel;

    public int Nivel { get => nivel; private set => nivel = value; }

    [SerializeField]
    private Sprite iconita;

    public Sprite Iconita { get => iconita; private set => iconita = value; }


}
