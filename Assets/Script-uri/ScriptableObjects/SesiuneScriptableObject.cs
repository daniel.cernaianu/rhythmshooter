using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//Enemies will spawn in "sessions". Each "session" is determined by the following information:
//Enemy: the enemy spawned in the session.
//Start: The time frame when the session starts.
//End: The time frame when the session ends.
//HP: Hit points of the enemy spawned in the session.
//Max: Max number of enemies from the session that can exist at once in the game. Does not affected by the modifier of Darkness 1+.
//SpawnCD/Cd: Number of seconds the game waits before trying to spawn more enemies from the session (up to the Max). Affected by Darkness 1+.
//Num p/spawn/Sp: Number of enemies from the session that are spawned at once, after every SpawnCD seconds, until they reach their Max number.
//For each currently active "spawn session", the game checks every SpawnCD seconds so see if there are already Max alive enemies. if not, it spawns Num p/spawn many (multiplied by the darkness modifier if applicable, rounded down if it's not an integer).
[CreateAssetMenu]
public class SesiuneScriptableObject: ScriptableObject
{

    public int numarSesiune;
    public InamicBaza inamic;
    public float timpStart;
    public float timpFinal;
    public float timpPanaLaGenerareInamici; //in secunde
    public int numarMaximInamici;
    public int numarInamiciDeGenerat;
    public float puncteXP;

}
