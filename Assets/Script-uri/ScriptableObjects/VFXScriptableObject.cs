using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

[CreateAssetMenu]
public class VFXScriptableObject : ScriptableObject
{
    public string Nume;
    public VisualEffect efect;
    public float durata;
}
