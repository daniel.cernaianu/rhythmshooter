using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu]

//Clasa scriptable-object care contine atributele de baza ale jucatorului
public class JucatorScriptableObject : ScriptableObject
{

    [SerializeField]
    private int puncteViataMaxime;

    public int PuncteViataMaxime { get => puncteViataMaxime; private set => puncteViataMaxime = value; }

    [SerializeField]
    private float vitezaMiscare;

    public float VitezaMiscare { get => vitezaMiscare; private set => vitezaMiscare = value; }

    //[SerializeField]
    //private float dauneBaza;

    //public float DauneBaza { get => dauneBaza; private set => dauneBaza = value; }

    [SerializeField]
    private float distantaEvadare;

    public float DistantaEvadare { get => distantaEvadare; private set => distantaEvadare = value; }

    //secunde
    [SerializeField]
    private float timpReactivareEvadare;

    public float TimpReactivareEvadare { get => timpReactivareEvadare; private set => timpReactivareEvadare = value; }

    [SerializeField]
    private float distantaMagnetSteluteXP;

    public float DistantaMagnetSteluteXP { get => distantaMagnetSteluteXP; private set => distantaMagnetSteluteXP = value; }

    //procente
    [SerializeField]
    private float bonusPutereDauneComboRitm;

    public float BonusPutereDauneComboRitm { get => bonusPutereDauneComboRitm; private set => bonusPutereDauneComboRitm = value; } 

}
