using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class AtributeScriptableObject : ScriptableObject
{
    [SerializeField]
    private List<string> listaAtribute;

    public List<string> ListaAtribute { get => listaAtribute; private set => listaAtribute = value; }
}
