using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class ArmamentScriptableObject : ScriptableObject
{
    [SerializeField] private Glont glontSpecific;
    public Glont GlontSpecific { get => glontSpecific; private set => glontSpecific = value; }

    [SerializeField] private PanouEchipament[] panouriSpecifice;
    public PanouEchipament[] PanouriSpecifice { get => panouriSpecifice; private set => panouriSpecifice = value; }

    [SerializeField] private Sprite iconita;

    public Sprite Iconita { get => iconita; private set => iconita = value; }

    [SerializeField] private string nume;

    public string Nume { get => nume; private set => nume = value; }

    [TextArea]
    [SerializeField] private string descriere;

    public string Descriere { get => descriere; private set => descriere = value; }

    [SerializeField] private float putere;
    public float Putere { get => putere; private set => putere = value; }

    [SerializeField] private float viteza;
    public float Viteza { get => viteza; private set => viteza = value; }

    [SerializeField] private float distanta;
    public float Distanta { get => distanta; private set => distanta = value; }

    [SerializeField] private int numarGloante;
    public int NumarGloante { get => numarGloante; private set => numarGloante = value; }


    [SerializeField] private int nivel;
    public int Nivel { get => nivel; private set => nivel = value; }


}
