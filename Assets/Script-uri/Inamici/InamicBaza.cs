using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InamicBaza : MonoBehaviour
{

    public Rigidbody2D inamicRigidBody;
    public Transform jucator;
    public InamicScriptableObject inamicScriptableObject;
    public float puncteViataCurente;
    public StelutaXP Steluta;
    public float BonusXP;
    public Collider2D colliderInamic;
    public event Action EsteDistrus;


    public void Moarte()
    {
        var stelutaGenerata = Instantiate(Steluta, transform.position, Quaternion.identity);
        stelutaGenerata.AdaugaMultiplicator(BonusXP);
        if(EsteDistrus != null)
        {
            EsteDistrus.Invoke();
        }
        
        Destroy(gameObject);
    }

    public void Ranit(float daune, Vector2 contact)
    {
        CatalogVFX.instanta.RuleazaVFX("lovitura", contact, Quaternion.identity);
        if (daune >= puncteViataCurente)
        {
            Moarte();
        }
        else if (puncteViataCurente > 0)
        {
            puncteViataCurente -= daune;
        }
       
    }




    // Start is called before the first frame update
    protected virtual void Start()
    {
        jucator = FindObjectOfType<Jucator>().transform;
        inamicRigidBody = GetComponent<Rigidbody2D>();
        colliderInamic = GetComponent<Collider2D>();
    }

    protected void Awake()
    {
        puncteViataCurente = inamicScriptableObject.puncteViata;
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        if (puncteViataCurente == 0)
            Moarte();
    }

    protected virtual void FixedUpdate()
    {
        var pozitieJucator = jucator.position;

        var directieJucator = CalculeazaDirectieJucator(pozitieJucator);

        if (ManipulatorSunet.instanta.Sincronizare())
        {
            ActualizeazaVitezaDeplasare(directieJucator);
        }
        else inamicRigidBody.velocity = Vector2.zero;

        transform.up = directieJucator;
    }


    public Vector2 CalculeazaDirectieJucator(Vector2 pozitieJucator)
    {
        var directie = pozitieJucator - (Vector2)transform.position;
        return directie.normalized;
    }

    public void ActualizeazaVitezaDeplasare(Vector2 directieVitezaDeplasareNoua)
    {
        if (inamicRigidBody.velocity != directieVitezaDeplasareNoua)
        {
            inamicRigidBody.velocity = directieVitezaDeplasareNoua * inamicScriptableObject.vitezaMiscare * Time.deltaTime;
        }
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
      
        if (collider.gameObject.CompareTag("Jucator"))
        {
            
            var contact = colliderInamic.ClosestPoint(collider.gameObject.transform.position);
            var jucator = collider.GetComponent<Jucator>();
            
            StartCoroutine(jucator.ReducePuncteViata(inamicScriptableObject.daune, contact, inamicScriptableObject.daune));
            

        }
    }

    
}
