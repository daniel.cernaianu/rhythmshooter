using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Cronometru : MonoBehaviour
{

    [SerializeField] private float timpCurent;
    [SerializeField] private float timpMaxim;


    public float TimpCurent { get; private set; }
    public float TimpMaxim { get; private set; }

    public event Action SchimbareTimp;

    // Update is called once per frame
    void Update()
    {
            TimpCurent += Time.deltaTime;
            
            ActualizeazaUI();
    }

    public string CalculeazaTimpCurent()
    {
        var timpMinute = Mathf.FloorToInt(TimpCurent / 60);
        
        var timpSecunde = Mathf.FloorToInt(TimpCurent % 60);

        var formatRezultat = string.Format("{0:00}:{1:00}", timpMinute, timpSecunde);
        return formatRezultat;
    }

    public void ActualizeazaUI()
    {
        SchimbareTimp.Invoke();
    }

}
