using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FundalInfinit : MonoBehaviour
{
    private float pozitieStartX;
    private float pozitieStartY;

    private float margineOrizontalaFundal;
    private float margineVerticalaFundal;

    [SerializeField] private GameObject cameraPrincipala;
    [SerializeField] private float vitezaDesfasurareFundal;

    private void Start()
    {
        pozitieStartX = transform.position.x;
        pozitieStartY = transform.position.y;
       
        margineOrizontalaFundal = GetComponent<SpriteRenderer>().bounds.size.x;
        margineVerticalaFundal = GetComponent<SpriteRenderer>().bounds.size.y;

    }

    private void Update()
    {
        var pozitieCameraX = cameraPrincipala.transform.position.x;
        var pozitieCameraY = cameraPrincipala.transform.position.y;

        var distantaDesfasurareX = cameraPrincipala.transform.position.x * vitezaDesfasurareFundal;
        var distantaDesfasurareY = cameraPrincipala.transform.position.y * vitezaDesfasurareFundal;

        transform.position = new Vector3(pozitieStartX + distantaDesfasurareX, pozitieStartY + distantaDesfasurareY, transform.position.z);

        if (pozitieCameraX > pozitieStartX + margineOrizontalaFundal)
        {
            pozitieStartX += margineOrizontalaFundal;
        }
        else if (pozitieCameraX < pozitieStartX - margineOrizontalaFundal)
        {
            pozitieStartX -= margineOrizontalaFundal;
        }

        if (pozitieCameraY > pozitieStartY + margineVerticalaFundal)
        {
            pozitieStartY  += margineVerticalaFundal;
        }
        else if (pozitieCameraY < pozitieStartY - margineVerticalaFundal)
        {
            pozitieStartY  -= margineVerticalaFundal;
        }

        
    }
}
