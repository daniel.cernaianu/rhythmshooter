using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StelutaXP : MonoBehaviour, IColectabil
{
    [SerializeField]
    private float valoareExperienta;
    private Rigidbody2D stelutaRigidbody;
    private Jucator jucator;
    private float putereAtractie;
    private bool EsteAtras;

    public void Awake()
    {
        stelutaRigidbody = GetComponent<Rigidbody2D>();
    }

    public void AdaugaMultiplicator(float experientaDeAdaugat)
    {
        valoareExperienta += experientaDeAdaugat;
    }

    public void EsteColectat(Jucator jucator,float putereAtractie)
    {
        EsteAtras = true;
        this.jucator = jucator;
        this.putereAtractie = putereAtractie;
        
    }

    public void Update()
    {
        if(EsteAtras == true)
        {
            var directieAtractie = (jucator.transform.position - transform.position).normalized;
            stelutaRigidbody.velocity = (directieAtractie * putereAtractie * Time.deltaTime);
        }
    }

    public void Efect(Jucator jucator)
    {
        jucator.AdaugaExperienta(valoareExperienta);
    }

    private void OnTriggerEnter2D(Collider2D coliziune)
    {
        if(coliziune.gameObject.CompareTag("Jucator"))
        {
            Efect(coliziune.GetComponent<Jucator>());
            Destroy(gameObject);
        }
    }

}
