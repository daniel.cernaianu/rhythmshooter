using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EchipamentPropulsie : EchipamentBaza
{

    public override void AplicaImbunatatiri()
    {
        jucator.VitezaMiscareCurenta *= 1f + echipamentAtributeBaza.Multiplicatoare[0] / 100f;
    }

    
}
