using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EchipamentDistantaEvadare : EchipamentBaza
{

    public override void AplicaImbunatatiri()
    {
        jucator.DistantaEvadareCurenta *= 1f + echipamentAtributeBaza.Multiplicatoare[0]  / 100f;
    }

}
