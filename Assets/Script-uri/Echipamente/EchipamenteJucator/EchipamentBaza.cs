using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EchipamentBaza : MonoBehaviour
{

    protected Jucator jucator;
    protected ArmamentBaza armament;
    public int NivelCurent;
    public EchipamentScriptableObject echipamentAtributeBaza;

    // Start is called before the first frame update
    void Start()
    {
        jucator = FindObjectOfType<Jucator>();
        armament = FindObjectOfType<ArmamentBaza>();
        AplicaImbunatatiri();
    }

    public void Awake()
    {
        NivelCurent = echipamentAtributeBaza.Nivel;
    }

    public virtual void AplicaImbunatatiri()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
