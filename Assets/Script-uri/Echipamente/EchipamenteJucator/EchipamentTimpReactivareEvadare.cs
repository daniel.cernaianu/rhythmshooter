using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EchipamentTimpReactivareEvadare : EchipamentBaza
{

    public override void AplicaImbunatatiri()
    {
        jucator.TimpReactivareEvadareCurent *= 1f - echipamentAtributeBaza.Multiplicatoare[0] / 100f;
    }


}
