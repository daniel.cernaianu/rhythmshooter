using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EchipamentMagnetism : EchipamentBaza
{


    public override void AplicaImbunatatiri()
    {
        jucator.DistantaMagnetSteluteXPCurenta *= 1f + echipamentAtributeBaza.Multiplicatoare[0] / 100f;
    }

}
