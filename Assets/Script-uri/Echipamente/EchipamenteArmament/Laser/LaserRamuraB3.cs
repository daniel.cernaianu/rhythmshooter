using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserRamuraB3 : EchipamentBaza
{
    //Rezistenta + 15%
    //Nr.Proiectile + 2


    public override void AplicaImbunatatiri()
    {
        ((ArmamentRazaLaser)armament).RezistentaLaser *= 1f + echipamentAtributeBaza.Multiplicatoare[0] / 100f;
        armament.NumarProiectileCurent += (int)(echipamentAtributeBaza.Multiplicatoare[1]);
    }

}
