using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserRamuraA3 : EchipamentBaza
{
    //Daune + 5%
    //Rezistenta + 20%


    public override void AplicaImbunatatiri()
    {
        armament.PutereDauneCurenta *= 1f + echipamentAtributeBaza.Multiplicatoare[0] / 100f;
        ((ArmamentRazaLaser)armament).RezistentaLaser *= 1f + echipamentAtributeBaza.Multiplicatoare[1] / 100f;
    }
}
