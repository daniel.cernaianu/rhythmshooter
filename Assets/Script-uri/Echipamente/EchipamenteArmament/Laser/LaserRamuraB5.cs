using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserRamuraB5 : EchipamentBaza
{
    //Daune + 10%
    //Nr.Proiectile +6


    [SerializeField]
    private ArmamentLaserSoare evolutieArmament;

    public ArmamentLaserSoare EvolutieArmament { get => evolutieArmament; private set => evolutieArmament = value; }

    public override void AplicaImbunatatiri()
    {
        armament.PutereDauneCurenta *= 1f + echipamentAtributeBaza.Multiplicatoare[0] / 100f;
        armament.NumarProiectileCurent += (int)(echipamentAtributeBaza.Multiplicatoare[1]);




        var atacJucator = jucator.gameObject.GetComponent<AtacJucator>();
        var armamentNou = Instantiate(evolutieArmament, jucator.transform);
        armament.TransferDate(armamentNou);

        atacJucator.ModificaArmamentEchipat(armamentNou);
        Destroy(armament.gameObject);
    }

}
