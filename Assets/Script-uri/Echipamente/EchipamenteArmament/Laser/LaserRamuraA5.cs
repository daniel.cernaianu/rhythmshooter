using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserRamuraA5 : EchipamentBaza
{
    //    Daune + 15%
    //Rezistenta + 30%


    [SerializeField]
    private ArmamentLaserDescompunere evolutieArmament;

    public ArmamentLaserDescompunere EvolutieArmament { get => evolutieArmament; private set => evolutieArmament = value; }

    public override void AplicaImbunatatiri()
    {
        armament.PutereDauneCurenta *= 1f + echipamentAtributeBaza.Multiplicatoare[0] / 100f;
        ((ArmamentRazaLaser)armament).RezistentaLaser *= 1f + echipamentAtributeBaza.Multiplicatoare[1] / 100f;




        var atacJucator = jucator.gameObject.GetComponent<AtacJucator>();
        var armamentNou = Instantiate(evolutieArmament, jucator.transform);
        armament.TransferDate(armamentNou);

        atacJucator.ModificaArmamentEchipat(armamentNou);
        Destroy(armament.gameObject);
    }

}
