using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserRamuraB4 : EchipamentBaza
{

//    Daune + 10%
//Rezistenta + 20%

    public override void AplicaImbunatatiri()
    {
        armament.PutereDauneCurenta *= 1f + echipamentAtributeBaza.Multiplicatoare[0] / 100f;
        ((ArmamentRazaLaser)armament).RezistentaLaser *= 1f + echipamentAtributeBaza.Multiplicatoare[1] / 100f;
    }
}
