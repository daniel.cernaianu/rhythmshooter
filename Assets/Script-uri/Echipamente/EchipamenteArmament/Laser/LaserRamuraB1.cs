using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserRamuraB1 : EchipamentBaza
{
    //Daune + 5%

    public override void AplicaImbunatatiri()
    {
        armament.PutereDauneCurenta *= 1f + echipamentAtributeBaza.Multiplicatoare[0] / 100f;
    }
}
