using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserRamuraB2 : EchipamentBaza
{
    //    Daune + 5%
    //Nr.Proiectile + 2

    public override void AplicaImbunatatiri()
    {
        armament.PutereDauneCurenta *= 1f + echipamentAtributeBaza.Multiplicatoare[0] / 100f;
        armament.NumarProiectileCurent += (int)(echipamentAtributeBaza.Multiplicatoare[1]);
    }

}
