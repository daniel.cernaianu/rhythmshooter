using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserRamuraA4 : EchipamentBaza
{
    //Rezistenta + 25%


    public override void AplicaImbunatatiri()
    {
        ((ArmamentRazaLaser)armament).RezistentaLaser *= 1f + echipamentAtributeBaza.Multiplicatoare[0] / 100f;
    }
}
