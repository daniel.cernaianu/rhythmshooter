using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlasmaRamuraA1 : EchipamentBaza
{

    //Daune + 15%

    public override void AplicaImbunatatiri()
    {
        armament.PutereDauneCurenta *= 1f + echipamentAtributeBaza.Multiplicatoare[0] / 100f;
    }

}
