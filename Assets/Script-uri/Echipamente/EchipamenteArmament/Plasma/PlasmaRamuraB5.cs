using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlasmaRamuraB5 : EchipamentBaza
{
    //Daune + 25%
    //Nr. Proiectile + 2

    [SerializeField]
    private ArmamentPlasmaPetale evolutieArmament;

    public ArmamentPlasmaPetale EvolutieArmament { get => evolutieArmament; private set => evolutieArmament = value; }

    public override void AplicaImbunatatiri()
    {
        armament.PutereDauneCurenta *= 1f + echipamentAtributeBaza.Multiplicatoare[0] / 100f;
        armament.NumarProiectileCurent += (int)(echipamentAtributeBaza.Multiplicatoare[1]);

        var atacJucator = jucator.gameObject.GetComponent<AtacJucator>();
        var armamentNou = Instantiate(evolutieArmament, jucator.transform);
        armament.TransferDate(armamentNou);

        atacJucator.ModificaArmamentEchipat(armamentNou);
        Destroy(armament.gameObject);
    }
}
