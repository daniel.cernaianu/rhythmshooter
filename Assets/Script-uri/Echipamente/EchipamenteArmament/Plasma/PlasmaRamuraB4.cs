using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlasmaRamuraB4 : EchipamentBaza
{
    //Daune + 15%
    //Nr. Proiectile + 2

    public override void AplicaImbunatatiri()
    {
        armament.PutereDauneCurenta *= 1f + echipamentAtributeBaza.Multiplicatoare[0] / 100f;
        armament.NumarProiectileCurent += (int)(echipamentAtributeBaza.Multiplicatoare[1]);
    }
}
