using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlasmaRamuraA4 : EchipamentBaza
{
    //Viteza Proiectile + 25%
    //Strapungere + 1

    public override void AplicaImbunatatiri()
    {
        armament.VitezaGlontCurenta *= 1f + echipamentAtributeBaza.Multiplicatoare[0] / 100f;
        ((ArmamentGlontPlasma)armament).Strapungere += (int)(echipamentAtributeBaza.Multiplicatoare[1]);
    }
}
