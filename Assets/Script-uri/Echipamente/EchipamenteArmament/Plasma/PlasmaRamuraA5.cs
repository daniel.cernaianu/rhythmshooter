using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlasmaRamuraA5 : EchipamentBaza
{
    //Daune + 35%
    //Strapungere + 2
    //Distanta Gloante + 25%

    [SerializeField]
    private ArmamentPlasmaInfometat evolutieArmament;

    public ArmamentPlasmaInfometat EvolutieArmament { get => evolutieArmament; private set => evolutieArmament = value; }


    public override void AplicaImbunatatiri()
    {
        armament.PutereDauneCurenta *= 1f + echipamentAtributeBaza.Multiplicatoare[0] / 100f;
        ((ArmamentGlontPlasma)armament).Strapungere += (int)(echipamentAtributeBaza.Multiplicatoare[1]);
        armament.DistantaGlontCurenta *= 1f + echipamentAtributeBaza.Multiplicatoare[2] / 100f;

        var atacJucator = jucator.gameObject.GetComponent<AtacJucator>();
        var armamentNou = Instantiate(evolutieArmament, jucator.transform);
        armament.TransferDate(armamentNou);

        atacJucator.ModificaArmamentEchipat(armamentNou);
        Destroy(armament.gameObject);
    }



}
