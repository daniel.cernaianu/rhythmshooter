using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlasmaRamuraA3 : EchipamentBaza
{
    //Daune + 20%
    //Strapungere + 1

    public override void AplicaImbunatatiri()
    {
        armament.PutereDauneCurenta *= 1f + echipamentAtributeBaza.Multiplicatoare[0] / 100f;
        ((ArmamentGlontPlasma)armament).Strapungere += (int)(echipamentAtributeBaza.Multiplicatoare[1]);
    }
}
