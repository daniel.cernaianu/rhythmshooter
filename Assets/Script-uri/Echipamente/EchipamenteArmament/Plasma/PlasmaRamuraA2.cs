using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlasmaRamuraA2 : EchipamentBaza
{
    //Daune + 15%
    //Distanta Gloante + 20%

    public override void AplicaImbunatatiri()
    {
        armament.PutereDauneCurenta *= 1f + echipamentAtributeBaza.Multiplicatoare[0] / 100f;
        armament.DistantaGlontCurenta *= 1f + echipamentAtributeBaza.Multiplicatoare[1] / 100f;
    }
}
