using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlasmaRamuraB3 : EchipamentBaza
{
    //Daune + 25%
    //Viteza Proiectilelor - 10%

    public override void AplicaImbunatatiri()
    {
        armament.PutereDauneCurenta *= 1f + echipamentAtributeBaza.Multiplicatoare[0] / 100f;
        armament.VitezaGlontCurenta *= 1f + echipamentAtributeBaza.Multiplicatoare[1] / 100f;
    }}
