using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlasmaRamuraB2 : EchipamentBaza
{
    //Nr. Proiectile + 2
    //Distanta Gloantelor - 10%

    public override void AplicaImbunatatiri()
    {
        armament.NumarProiectileCurent += (int)(echipamentAtributeBaza.Multiplicatoare[0]);
        armament.DistantaGlontCurenta *= 1f + echipamentAtributeBaza.Multiplicatoare[1] / 100f;
    }
}
