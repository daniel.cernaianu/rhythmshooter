using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EchipamentDistantaGlont : EchipamentBaza
{

    public override void AplicaImbunatatiri()
    {
        armament.DistantaGlontCurenta *= 1f + echipamentAtributeBaza.Multiplicatoare[0] / 100f;
    }

}
