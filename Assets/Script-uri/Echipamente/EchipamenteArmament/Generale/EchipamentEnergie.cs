using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EchipamentEnergie : EchipamentBaza
{


    public override void AplicaImbunatatiri()
    {
        armament.PutereDauneCurenta *= 1f + echipamentAtributeBaza.Multiplicatoare[0] / 100f;
    }

    
}
