using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EchipamentDispersie : EchipamentBaza
{

    public override void AplicaImbunatatiri()
    {
        armament.NumarProiectileCurent += (int)echipamentAtributeBaza.Multiplicatoare[0];
    }

    
}
