using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EchipamentAccelerator : EchipamentBaza
{

    public override void AplicaImbunatatiri()
    {
        armament.VitezaGlontCurenta *= 1f + echipamentAtributeBaza.Multiplicatoare[0] / 100f;
    }
    
}
