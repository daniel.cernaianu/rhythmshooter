using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RacheteRamuraB4 : EchipamentBaza
{
    //Distanta Glont + 20%
    //Daune Explozie + 25%

    public override void AplicaImbunatatiri()
    {
        armament.DistantaGlontCurenta *= 1f + echipamentAtributeBaza.Multiplicatoare[0] / 100f;
        ((ArmamentAruncatorRachete)armament).DauneExplozie *= 1f + echipamentAtributeBaza.Multiplicatoare[1] / 100f;
    }
}
