using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RacheteRamuraA2 : EchipamentBaza
{

    //Daune + 15%
    //Viteza Proiectile + 15%
    public override void AplicaImbunatatiri()
    {
        armament.PutereDauneCurenta *= 1f + echipamentAtributeBaza.Multiplicatoare[0] / 100f;
        armament.VitezaGlontCurenta *= 1f + echipamentAtributeBaza.Multiplicatoare[1] / 100f;
    }
}
