using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RacheteRamuraA5 : EchipamentBaza
{
    //Daune + 25%
    //Viteza Proiectile + 25%

    [SerializeField]
    private ArmamentRacheteIzbucnire evolutieArmament;

    public ArmamentRacheteIzbucnire EvolutieArmament { get => evolutieArmament; private set => evolutieArmament = value; } 

    public override void AplicaImbunatatiri()
    {
        armament.PutereDauneCurenta *= 1f + echipamentAtributeBaza.Multiplicatoare[0] / 100f;
        armament.VitezaGlontCurenta *= 1f + echipamentAtributeBaza.Multiplicatoare[1] / 100f;

        
        

        var atacJucator = jucator.gameObject.GetComponent<AtacJucator>();
        var armamentNou = Instantiate(evolutieArmament, jucator.transform);
        armament.TransferDate(armamentNou);

        atacJucator.ModificaArmamentEchipat(armamentNou);
        Destroy(armament.gameObject);
    }
}
