using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RacheteRamuraA3 : EchipamentBaza
{
    //Viteza Proiectile + 20%
    //Distanta + 15%

    public override void AplicaImbunatatiri()
    {
        armament.VitezaGlontCurenta *= 1f + echipamentAtributeBaza.Multiplicatoare[0] / 100f;
        armament.DistantaGlontCurenta *= 1f + echipamentAtributeBaza.Multiplicatoare[1] / 100f;
    }
}
