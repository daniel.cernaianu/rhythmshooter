using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RacheteRamuraA4 : EchipamentBaza
{
    //Daune + 15%
    //Daune Epxlozie + 25%

    public override void AplicaImbunatatiri()
    {
        armament.PutereDauneCurenta *= 1f + echipamentAtributeBaza.Multiplicatoare[0] / 100f;
        ((ArmamentAruncatorRachete)armament).DauneExplozie *= 1f + echipamentAtributeBaza.Multiplicatoare[1] / 100f;
    }
}
