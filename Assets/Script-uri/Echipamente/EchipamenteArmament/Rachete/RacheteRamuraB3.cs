using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RacheteRamuraB3 : EchipamentBaza
{
    //Nr. Proiectile + 2
    //Raza Explozie + 30%
    public override void AplicaImbunatatiri()
    {
        armament.NumarProiectileCurent += (int)(echipamentAtributeBaza.Multiplicatoare[0]);
        ((ArmamentAruncatorRachete)armament).RazaExplozie *= 1f + echipamentAtributeBaza.Multiplicatoare[1] / 100f;
    }
}
