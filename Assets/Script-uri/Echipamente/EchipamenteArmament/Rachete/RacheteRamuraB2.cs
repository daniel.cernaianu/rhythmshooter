using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RacheteRamuraB2 : EchipamentBaza
{
    //Nr. Proiectile + 2
    //Daune Explozie + 20%

    public override void AplicaImbunatatiri()
    {
        armament.NumarProiectileCurent += (int)(echipamentAtributeBaza.Multiplicatoare[0]);
        ((ArmamentAruncatorRachete)armament).DauneExplozie *= 1f + echipamentAtributeBaza.Multiplicatoare[1] / 100f;
    }
}
