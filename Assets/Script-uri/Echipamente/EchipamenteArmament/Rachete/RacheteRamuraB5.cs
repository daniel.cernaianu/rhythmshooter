using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RacheteRamuraB5 : EchipamentBaza
{
    //Nr. Proiectile + 2
    //Raza Explozie + 50%


    [SerializeField]
    private ArmamentRacheteHaos evolutieArmament;

    public ArmamentRacheteHaos EvolutieArmament { get => evolutieArmament; private set => evolutieArmament = value; }

    public override void AplicaImbunatatiri()
    {
        armament.NumarProiectileCurent += (int)(echipamentAtributeBaza.Multiplicatoare[0]);
        ((ArmamentAruncatorRachete)armament).RazaExplozie *= 1f + echipamentAtributeBaza.Multiplicatoare[1] / 100f;

        var atacJucator = jucator.gameObject.GetComponent<AtacJucator>();
        var armamentNou = Instantiate(evolutieArmament, jucator.transform);
        armament.TransferDate(armamentNou);

        atacJucator.ModificaArmamentEchipat(armamentNou);
        Destroy(armament.gameObject);

    }
}
