using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IVisitor
{
    void Visit(ArmamentGlontPlasma obj);
    void Visit(ArmamentRazaLaser obj);
    void Visit(ArmamentAruncatorRachete obj);
    void Visit(ArmamentBaza armamentBaza);
}
