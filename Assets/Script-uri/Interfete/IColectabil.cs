using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IColectabil
{
    public void EsteColectat(Jucator jucator, float putereAtractie);
    public void Efect(Jucator jucator);
    public void AdaugaMultiplicator(float multiplicator);
}
