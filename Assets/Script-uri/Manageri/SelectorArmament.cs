using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectorArmament : MonoBehaviour
{

    public static SelectorArmament instanta;
    public ArmamentBaza armamentAles;

    private void Awake()
    {
        if(instanta == null)
        {
            instanta = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Debug.LogWarning("STERS INSTANTA EXTRA DE SELECTOR");
            Destroy(gameObject);
        }
    }

    public static ArmamentBaza ObtineArmamentAles()
    {
        return instanta.armamentAles;
    }

    public void DestroySingleton()
    {
        instanta = null;
        Destroy(gameObject);
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
