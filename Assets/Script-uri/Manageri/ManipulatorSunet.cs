using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

//SINGLETON
public class ManipulatorSunet : MonoBehaviour
{
    #region proprietati-generale


    [SerializeField] AudioSource muzicaFundal;


    [SerializeField] private float DSPStartMuzica;

    [SerializeField] private float marjaEroareSincronizareActiuni;

    #endregion

    #region proprietati-specifice


    [SerializeField] private float BPM;

    [SerializeField] private float latenta;


    [SerializeField] private  float secPeBataie;

 
    [SerializeField] private  int pozitieInBatai;


    [SerializeField] private  float pozitieInSecunde;

    public float UltimaActiune { get; set; } = 0f;

    public float UrmatoareaBataie { get; set; }

    public float UltimaActiunePrecisa { get; set; }

    public event Action BataieSuccesata;

    private float ultimaBataie;


    #endregion

    #region proprietati-pozitie-cantec
    public int PozitieInBatai { get => pozitieInBatai; set => pozitieInBatai = value; }
    public float PozitieInSecunde { get => pozitieInSecunde; set => pozitieInSecunde = value; }
    public float SecPeBataie { get => secPeBataie; set => secPeBataie = value; }
    #endregion

    #region instanta-singleton
    public static ManipulatorSunet instanta;
    


    private void Awake()
    {
        if (instanta == null)
            instanta = this;
    }
    #endregion

    private void Start()
    {
        ultimaBataie = 0f;
        SecPeBataie = 60f / BPM;
        DSPStartMuzica = (float)AudioSettings.dspTime;
        muzicaFundal.Play();
    }

    private void Update()
    {       
        PozitieInSecunde = (float)(AudioSettings.dspTime - DSPStartMuzica) * muzicaFundal.pitch - latenta;

        PozitieInBatai = (int)(PozitieInSecunde / SecPeBataie);

        UrmatoareaBataie = ultimaBataie + SecPeBataie;
        
        if (PozitieInSecunde >  UrmatoareaBataie)
        {
            ActualizeazaUI();
            ultimaBataie += SecPeBataie;
        }
    }


    public bool Sincronizare(out string NaturaRezultat)
    {
        UltimaActiunePrecisa = PozitieInSecunde;

        var timpActiuneTimpUrmBataieDelta = Mathf.Abs(UltimaActiunePrecisa + SecPeBataie - UrmatoareaBataie);

        if (timpActiuneTimpUrmBataieDelta <= marjaEroareSincronizareActiuni && timpActiuneTimpUrmBataieDelta > 0f)
        {
            NaturaRezultat = "PERFECT";
            return true;
        }

        else if (timpActiuneTimpUrmBataieDelta <= marjaEroareSincronizareActiuni && timpActiuneTimpUrmBataieDelta < 0f)
        {
            NaturaRezultat = "DEVREME";
            return false;
        }

        else if (timpActiuneTimpUrmBataieDelta > marjaEroareSincronizareActiuni && timpActiuneTimpUrmBataieDelta > 0f)
        {
            NaturaRezultat = "TARZIU";
            return false;
        }

        NaturaRezultat = "";
        return false;
        
    }

    public bool Sincronizare()
    {
        UltimaActiunePrecisa = PozitieInSecunde;

        var timpActiuneTimpUrmBataieDelta = Mathf.Abs(UltimaActiunePrecisa + SecPeBataie - UrmatoareaBataie);

        if (timpActiuneTimpUrmBataieDelta <= marjaEroareSincronizareActiuni && timpActiuneTimpUrmBataieDelta > 0f)
        {
            
            return true;
        }

        else if (timpActiuneTimpUrmBataieDelta <= marjaEroareSincronizareActiuni && timpActiuneTimpUrmBataieDelta < 0f)
        {
            
            return false;
        }

        else if (timpActiuneTimpUrmBataieDelta > marjaEroareSincronizareActiuni && timpActiuneTimpUrmBataieDelta > 0f)
        {
            
            return false;
        }

        
        return false;

    }



    public void ActualizeazaUI()
    {
        BataieSuccesata.Invoke();
    }

}
