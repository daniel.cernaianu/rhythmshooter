using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutomatStari
{

    public IStare StareCurenta { get; private set; }

    public StareDerulare derulare;
    public StareEcranInformatii informatii;
    public StareNivelCrescut urcareNivel;
    public StareInceput inceput;
    public StareCastigare castigare; 

    public event Action<IStare> modificareStare;


    public AutomatStari(ManagerJoc manager)
    {
        inceput = new StareInceput(manager);
        derulare = new StareDerulare(manager);
        urcareNivel = new StareNivelCrescut(manager);
        informatii = new StareEcranInformatii(manager);
        castigare = new StareCastigare(manager);
    }

    public void Init(IStare stare)
    {
        StareCurenta = stare;

        stare.Intra();

        modificareStare?.Invoke(stare);
    }

    public void TreciLaStarea(IStare stare)
    {
        StareCurenta.Iesi();

        StareCurenta = stare;

        stare.Intra();

        modificareStare?.Invoke(stare);
    }

    public void Actualizeaza()
    {
        if(StareCurenta != null)
        {
            StareCurenta.Actualizeaza();
        }
    }

}
