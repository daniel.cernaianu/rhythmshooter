using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AfisajStatus : MonoBehaviour
{

    private Jucator jucatorCurent;
    private AtacJucator atacJucatorCurent;

    public Jucator JucatorCurent { get => jucatorCurent; private set => jucatorCurent = value; }
    public AtacJucator AtacJucatorCurent { get => atacJucatorCurent; private set => atacJucatorCurent = value; }

    public event Action ActualizareAfisaj;



    // Start is called before the first frame update
    void Start()
    {
        JucatorCurent = FindObjectOfType<Jucator>();
        AtacJucatorCurent = FindObjectOfType<AtacJucator>();
        
    }




    public void Activeaza()
    {
        ActualizareAfisaj.Invoke();
    }


    


    
}
