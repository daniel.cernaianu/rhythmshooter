using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SelectorEchipament : MonoBehaviour
{

    
    public List<PanouEchipament> EchipamenteDeAles = new List<PanouEchipament>(3);
    [SerializeField]
    private ManagerInventar inventar;

    public ManagerInventar Inventar { get => inventar; private set => inventar = value; }

    [SerializeField]
    private List<PanouEchipament> EchipamenteArmament;
    [SerializeField]
    private List<PanouEchipament> EchipamenteGenerale;

    public event Action<bool> AlegereEchipament; 

    public List<PanouEchipament> EchipamentePosibile;

    public bool EsteActivat = false;

    private bool specificArmament = false;

    public PanouEchipament panouAles { get; set; }

    private void Start()
    {
        foreach(PanouEchipament panou in EchipamenteGenerale)
        {
            panou.Echipament.NivelCurent = 1;
        }
    }

    private void OnDestroy()
    {
        foreach (PanouEchipament panou in EchipamenteGenerale)
        {
            panou.Echipament.NivelCurent = 1;
        }
    }

    
    public void FormeazaEchipamentePosibile()
    {

        EchipamentePosibile.Clear();

        if (Inventar.EsteLaCapacitateMaxima())
        {
            EliminaPanouriExtra();
        }

        EchipamentePosibile.AddRange(EchipamenteGenerale);
        EchipamentePosibile.Add(EchipamenteArmament.OrderBy(panou => panou.NivelEchipament()).FirstOrDefault());
        
        SeteazaEchipamenteleAleatorii();
    }

    public void ObtineEchipamenteArmament(PanouEchipament panou)
    {
        if (specificArmament)
            EchipamenteArmament.RemoveAll(p => !p.NumeEchipament().Equals(panou.NumeEchipament()));
    }

    public void EliminaEchipamenteNepotrivite()
    {
        if(panouAles != null)
        {
            if(panouAles.Echipament.echipamentAtributeBaza.SpecificArmament)
            {
                EchipamenteArmament.RemoveAll(panou => panou.NivelEchipament() == panouAles.NivelEchipament() && panou.NumeEchipament().Equals(panouAles.NumeEchipament()));
            }
               
            else
            {
                if(panouAles.Echipament.NivelCurent == 5 )
                {
                    EchipamenteGenerale.RemoveAll(panou => panou.NumeEchipament().Equals(panouAles.NumeEchipament()));
                }
                
            }
        }
    }

    public void EliminaPanouriExtra()
    {
        var listaPanouriInInventar = Inventar.EchipamenteGenerale.Select(el => el.echipament.echipamentAtributeBaza.Nume).ToList();

        List<PanouEchipament> ColectieTemporara = new List<PanouEchipament>();
        ColectieTemporara.AddRange(EchipamenteGenerale);

        foreach(PanouEchipament panou in ColectieTemporara)
        {
            if(!listaPanouriInInventar.Exists(x => x.Equals(panou.Echipament.echipamentAtributeBaza.Nume)))
            {
                EchipamenteGenerale.RemoveAll(p => p.NumeEchipament().Equals(panou.NumeEchipament()));
            }
        }
    }

    public void SeteazaEchipamenteleAleatorii()
    {
        EchipamenteDeAles.Clear();
        if(EchipamentePosibile.Count() > 0)
        {
            var numereAleatorii = NumereAleatoriiDistincte(3);

            EchipamenteDeAles.Add(EchipamentePosibile[numereAleatorii[0]]);
            EchipamenteDeAles.Add(EchipamentePosibile[numereAleatorii[1]]);
            EchipamenteDeAles.Add(EchipamentePosibile[numereAleatorii[2]]);
        }
    }

    public int[] NumereAleatoriiDistincte(int numere)
    {
        HashSet<int> multimeNumere = new HashSet<int>();

        while(multimeNumere.Count() < numere)
        {
            int numarAleatoriu = UnityEngine.Random.Range(0, EchipamentePosibile.Count());
            multimeNumere.Add(numarAleatoriu);
        }

        return multimeNumere.ToArray();
    }

    public void PermiteAlegereaEchipamentului(bool specificArmament)
    {
        this.specificArmament = specificArmament;
        EsteActivat = true;
        AlegereEchipament.Invoke(specificArmament);
    }

    public void ActualizeazaNiveluriEchipamentePosibile()
    {
        if(!specificArmament)
        {
            var echipament = EchipamenteGenerale.Find(p => p.NumeEchipament().Equals(panouAles.NumeEchipament()));
            if (echipament != null)
            {
                echipament.Echipament.NivelCurent++;
            }
        } 
    }

    public void EchipeazaEchipamentulAles()
    {
        Inventar.AdaugaEchipament(panouAles.Echipament);
        
    }

    

}
