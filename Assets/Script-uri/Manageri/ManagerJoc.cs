using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class ManagerJoc : MonoBehaviour
{

    [SerializeField] private Jucator jucator;
    [SerializeField] private GeneratorInamici generator;
    [SerializeField] private Cronometru cronometru;
    [SerializeField] private SelectorEchipament selectorEchipament;
    [SerializeField] private AfisajStatus afisajStatusCurent;
    [SerializeField] private GameObject afisajInventar;
    [SerializeField] private ManagerScene managerScene;

    public SelectorEchipament Selector => selectorEchipament;
    

    private AutomatStari automat;
    public AutomatStari Automat => automat;

    public AfisajStatus AfisajStatusCurent { get => afisajStatusCurent; private set => afisajStatusCurent = value; }

    public bool InventarActiv;

    public static Vector3 pozitieJucator;
    public event Action EventCrestereNivel;

    private void Awake()
    {
        automat = new AutomatStari(this);
    }
    // Start is called before the first frame update
    void Start()
    {
        automat.Init(automat.inceput);
        jucator.CresteNivel += ProceseazaCrestereNivel;
        jucator.Moarte += RestarteazaJoc;
    }

    private void RestarteazaJoc()
    {
        managerScene.SchimbaScena(1);
    }


    // Update is called once per frame
    void Update()
    {
        
        pozitieJucator = jucator.transform.position;
        
        
        if(cronometru.TimpCurent / 60f >= 15f)
        {
            managerScene.SchimbaScena(1);
        }
        Automat.Actualizeaza();
    }

    public void OnDestroy()
    {
        if(jucator != null)
            jucator.CresteNivel -= ProceseazaCrestereNivel;
    }

    public void ProceseazaCrestereNivel(int nivel)
    {
        
             if (nivel == 2)
             {
                 selectorEchipament.PermiteAlegereaEchipamentului(true);
             }
             else if(jucator.Inventar.EsteJucatorulLaNivelMaximDeEchipamente())
             {
                jucator.RestaureazaPuncteViata(1f);
             }
             else
             {
                 selectorEchipament.PermiteAlegereaEchipamentului(false);
             }
         
    }

    public void AfiseazaStatus(InputAction.CallbackContext context)
    {
        if(context.started)
        {
            if(afisajInventar.activeInHierarchy)
            {
                afisajInventar.SetActive(false);
                InventarActiv = false;
            }
            else
            {
                afisajInventar.SetActive(true);
                AfisajStatusCurent.Activeaza();
                InventarActiv = true;
            }
        }
    }

    

}
