using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public static class ManagerEvenimente
{

    public static bool Sincronizat { get; set; }
    
    public static event UnityAction ComandaInputSincronizabila;

    public static event UnityAction Bataie;

    public static void InputSincronizabil() => ComandaInputSincronizabila?.Invoke();

    public static void PeBataie() => Bataie?.Invoke();

}
