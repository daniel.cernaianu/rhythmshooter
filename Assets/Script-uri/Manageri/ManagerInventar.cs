using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//MODEL
public class ManagerInventar : MonoBehaviour
{

    public ArmamentBaza armaEchipata;
    public List<ElementInventar> EchipamenteGenerale = new List<ElementInventar>();
   
    public ElementInventar RamuraArmament;

    [SerializeField]private int dimensiuneInventar;

    public int DimensiuneInventar { get => dimensiuneInventar; private set => dimensiuneInventar = value; }

    public static int pozitie = 0;

    public event Action<EchipamentBaza> AdaugareEchipament;
    public event Action<EchipamentBaza> AdaugareEchipamentGeneralInInventar;
    public event Action<EchipamentBaza> AdaugareEchipamentSpecificArmament;
    public event Action Initializare;

    public event Action<int> ActualizeazaEchipamentGeneralInInventar;
    public event Action ActualizeazaEchipamentSpecificArmament;

    [System.Serializable]
    public class ElementInventar
    {
        public EchipamentBaza echipament;
        public int nivel = 1;

        public ElementInventar(EchipamentBaza echipament, int nivel)
        {
            this.echipament = echipament;
            this.nivel = nivel;
        }

        public ElementInventar()
        {

        }

        public bool Equals(ElementInventar AltElement)
        {
            if (echipament.echipamentAtributeBaza.Nume.Equals(AltElement.echipament.echipamentAtributeBaza.Nume))
            {
                return true;
            }
            return false;
        }

        public Sprite ObtineIconitaEchipament()
        {
            return echipament.echipamentAtributeBaza.Iconita;
        }

    }

    public void AdaugaEchipament(EchipamentBaza echipament)
    {
        if(echipament.echipamentAtributeBaza.SpecificArmament)
        {
            if(RamuraArmament.echipament == null)
            {
                RamuraArmament.echipament = echipament;
                RamuraArmament.nivel = echipament.NivelCurent;
                AdaugareEchipamentSpecificArmament.Invoke(echipament);
            }
            else
            {

                RamuraArmament.nivel++;
                ActualizeazaEchipamentSpecificArmament.Invoke();
            }
            
        }
        else
        {

            ElementInventar elementNou = new(echipament, echipament.NivelCurent);


            if (Exista(elementNou))
            {
                var pozitieElementExistent = ObtinePozitieElementInventar(elementNou);
                EchipamenteGenerale[pozitieElementExistent].nivel = elementNou.nivel;

                ActualizeazaEchipamentGeneralInInventar.Invoke(pozitieElementExistent);
            }
            else if(!EsteLaCapacitateMaxima())
            {
                EchipamenteGenerale.Add(elementNou);
                pozitie++;

                AdaugareEchipamentGeneralInInventar.Invoke(echipament);
            }

            
        }

        AdaugareEchipament.Invoke(echipament);
    }

    public bool Exista(ElementInventar element)
    {
        foreach (ElementInventar elm in EchipamenteGenerale)
        {
            if (elm.Equals(element))
            {
                return true;
            }
        }
        return false;
    }

    public bool EsteJucatorulLaNivelMaximDeEchipamente()
    {
        bool rezultat = true;
        foreach(ElementInventar elm in EchipamenteGenerale)
        {
            if (elm.nivel < 5)
            {
                rezultat = false;
            }
        }
        if(RamuraArmament.nivel < 5)
        {
            rezultat = false;
        }
        return rezultat;
    }

    //public void DebugDisplay()
    //{
    //    foreach(ElementInventar elm in EchipamenteGenerale)
    //    {
    //        Debug.Log(elm.echipament.echipamentAtributeBaza.Nume);
    //        Debug.Log(elm.nivel);
    //    }
    //    Debug.Log(RamuraArmament.echipament.echipamentAtributeBaza.Nume);
    //    Debug.Log(RamuraArmament.nivel);
    //}

    public int ObtinePozitieElementInventar(ElementInventar element)
    {
        if (Exista(element))
        {
            for(int i = 0; i <= pozitie; i++)
            {
                if(EchipamenteGenerale[i].Equals(element))
                {
                    return i;
                }
            }
        }
        return -1;
    }

    public void SeteazaArmament(ArmamentBaza armament)
    {
        armaEchipata = armament;
    }

    public ArmamentBaza ObtineArmament()
    {
        return armaEchipata;
    }

    public void InitializeazaInventar(ArmamentBaza armament)
    {
        EchipamenteGenerale = new List<ElementInventar>(dimensiuneInventar);

        SeteazaArmament(armament);

        Initializare.Invoke();
    }

    public bool EsteLaCapacitateMaxima()
    {
        if (pozitie == dimensiuneInventar)
            return true;
        return false;
    }

    // Start is called before the first frame update
    void Start()
    {
        pozitie = 0;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
