using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class ManagerComenzi : MonoBehaviour
{

    public static ManagerComenzi instanta;
    public static PlayerInput comenziJucator;
    


    private void Awake()
    {
        if(instanta == null)
        {
            instanta = this;
        }

        comenziJucator = GetComponent<PlayerInput>();
    }


    public static void ModificaProfilDeActiuniComenziJucator(string actionMap)
    {
        comenziJucator.SwitchCurrentActionMap(actionMap);
    }

}
