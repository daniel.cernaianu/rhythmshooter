using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneratorInamici : MonoBehaviour
{

    [SerializeField] private Cronometru cronometru;

    public SesiuneScriptableObject[] sesiuniGenerare;
    public int indexSesiuneCurenta;
    public SesiuneScriptableObject sesiuneCurenta;
    public static int numarInamiciGeneratSesiuneCurenta;
    public float timpGenerare = 0f;


    // Start is called before the first frame update
    void Start()
    {
        indexSesiuneCurenta = 0;
        numarInamiciGeneratSesiuneCurenta = 0;
        
    }

    // Update is called once per frame
    void Update()
    {
        if(sesiuniGenerare[indexSesiuneCurenta] != null)
        {
            sesiuneCurenta = sesiuniGenerare[indexSesiuneCurenta];
            if (sesiuneCurenta.timpFinal <= cronometru.TimpCurent)
            {
                indexSesiuneCurenta++;
            }
        }

        timpGenerare += Time.deltaTime;

        if(timpGenerare >= sesiuneCurenta.timpPanaLaGenerareInamici)
        {
            GenereazaInamici();
            timpGenerare = 0f;
        }

        
    }

    public void GenereazaInamici()
    {
        var pozitieJucator = (Vector2)ManagerJoc.pozitieJucator;
        var numarInamiciDeGenerat = sesiuneCurenta.numarInamiciDeGenerat;
        if(numarInamiciGeneratSesiuneCurenta >= sesiuneCurenta.numarMaximInamici)
        {
            numarInamiciDeGenerat = 1;
        }
        for (int i = 0; i < sesiuneCurenta.numarInamiciDeGenerat; i++)
        {
            var inamic = Instantiate(sesiuneCurenta.inamic, CalculeazaPozitieUrmatoare(pozitieJucator), Quaternion.identity);
            inamic.BonusXP = sesiuneCurenta.puncteXP;

            numarInamiciGeneratSesiuneCurenta++;
        }
    }

    public Vector2 CalculeazaPozitieUrmatoare(Vector2 pozitieJucator)
    {

        int[] directii = { -1, 1 };
        

        var offsetValoareOrizontal = Random.Range(10f, 14f);
        var offsetValoareVertical = Random.Range(5f, 9f);


        return new Vector2(pozitieJucator.x + directii[Random.Range(0, 2)] * offsetValoareOrizontal, pozitieJucator.y + directii[Random.Range(0, 2)] * offsetValoareVertical);
    }

}
