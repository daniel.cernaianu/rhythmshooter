using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StareInceput : IStare
{

    public ManagerJoc manager;

    public StareInceput(ManagerJoc manager)
    {
        this.manager = manager;
    }


    public void Actualizeaza()
    {
        manager.Automat.TreciLaStarea(manager.Automat.derulare);
    }

    public void Iesi()
    {
        
    }

    public void Intra()
    {
        
    }
}
