using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StareDerulare : IStare
{


    public ManagerJoc manager;

    public StareDerulare(ManagerJoc manager)
    {
        this.manager = manager;
    }


    public void Actualizeaza()
    {
        if(manager.Selector.EsteActivat)
        {
            manager.Automat.TreciLaStarea(manager.Automat.urcareNivel);
        }
        else if(manager.InventarActiv)
        {
            manager.Automat.TreciLaStarea(manager.Automat.informatii);
        }
    }

    public void Iesi()
    {
        
    }

    public void Intra()
    {
        
    }
}
