using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StareNivelCrescut : IStare
{

    public ManagerJoc manager;
    public SelectorEchipament selector;


    public StareNivelCrescut(ManagerJoc manager)
    {
        this.manager = manager;
        selector = manager.Selector;

    }

    public void Actualizeaza()
    {
        if(selector.panouAles != null)
        {
            manager.Automat.TreciLaStarea(manager.Automat.derulare);
        }
        else if(manager.InventarActiv)
        {
            manager.Automat.TreciLaStarea(manager.Automat.informatii);
        }
    }

    public void Iesi()
    {
        if(Time.timeScale != 1f)
        {
            Time.timeScale = 1f;
            ManagerComenzi.ModificaProfilDeActiuniComenziJucator("Player");
        }
        
    }

    public void Intra()
    {
        Time.timeScale = 0f;
        ManagerComenzi.ModificaProfilDeActiuniComenziJucator("UI");
    }
}
