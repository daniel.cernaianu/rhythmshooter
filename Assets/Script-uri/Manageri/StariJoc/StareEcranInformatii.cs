using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StareEcranInformatii : IStare
{
    private ManagerJoc manager;



    public StareEcranInformatii(ManagerJoc manager)
    {
        this.manager = manager;
    }

    public void Actualizeaza()
    {
       if(!manager.InventarActiv)
       {
            if (manager.Selector.EsteActivat)
            {
                manager.Automat.TreciLaStarea(manager.Automat.urcareNivel);
            }
            else
            {
                manager.Automat.TreciLaStarea(manager.Automat.derulare);
            }
        }
    }

    public void Iesi()
    {
        if (Time.timeScale != 1f)
        {
            Time.timeScale = 1f;
            ManagerComenzi.ModificaProfilDeActiuniComenziJucator("Player");
        }

    }

    public void Intra()
    {
        Time.timeScale = 0f;
        ManagerComenzi.ModificaProfilDeActiuniComenziJucator("UI");
    }
}
