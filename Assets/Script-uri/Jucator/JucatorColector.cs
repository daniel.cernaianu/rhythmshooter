using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JucatorColector : MonoBehaviour
{
    private CircleCollider2D coliziune;
    private Jucator jucator;
    [SerializeField] private float putereAtractie;

    private void Start()
    {
        jucator = FindObjectOfType<Jucator>();
        coliziune = GetComponent<CircleCollider2D>();
    }


    private void Update()
    {
        coliziune.radius = jucator.DistantaMagnetSteluteXPCurenta;
    }

    private void OnTriggerEnter2D(Collider2D coliziune2D)
    {
        if(coliziune2D.gameObject.TryGetComponent(out IColectabil colectabil))
        {
            

            colectabil.EsteColectat(jucator, putereAtractie);
        }
    }
}
