using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class DeplasareJucator : MonoBehaviour
{

    [SerializeField] private float durataEvadare;
    [SerializeField] private float vitezaIncetinita;
    [SerializeField] private float durataIncetinire;
    [SerializeField] private float durataImpingere;

    private TrailRenderer animatie;
    private Rigidbody2D rigidBody;
    private Jucator atributeJucator;
    private Vector2 contactImpingere;
    private float vitezaImpingere;
    private bool EsteInImpingere = false;

    public bool PoateActionaEvadare { get; set; } = true;
    public bool EsteInEvadare { get; set; } = false;

    public float DurataIncetinire { get => durataIncetinire; private set => durataIncetinire = value; }
    public Vector2 VectorDeplasare { get; set; }
    public Vector2 VectorRotatie { get; set; }

    public event Action StartIncetinire;
    public event Action StopIncetinire;
    public event Action StartIncarcareEvadare;
    public event Action StopIncarcareEvadare;


    public void FixedUpdate()
    {
        if (!EsteInEvadare)
        {
            if(!EsteInImpingere)
            {
                rigidBody.velocity = VectorDeplasare * atributeJucator.VitezaMiscareCurenta * Time.deltaTime;
            }     
            else
            {
                rigidBody.velocity = Vector2.zero;
                rigidBody.AddForce(-contactImpingere * vitezaImpingere, ForceMode2D.Impulse);
            }
        }
            
    }

    void Start()
    {
        rigidBody = GetComponent<Rigidbody2D>();
        animatie = GetComponent<TrailRenderer>();
        atributeJucator = GetComponent<Jucator>();
        atributeJucator.IaDaune += PornesteImpingere;
    }

    private void OnDestroy()
    {
        atributeJucator.IaDaune -= PornesteImpingere;
    }

    void Update()
    {
        
    }

    public void Rotatie()
    {
        var direction = VectorRotatie - (Vector2)transform.position;
        transform.up = direction;
       
    }

    public IEnumerator ActioneazaEvadare()
    {
        PoateActionaEvadare = false;
        EsteInEvadare = true;
        gameObject.layer = 8;
        rigidBody.velocity = VectorDeplasare * atributeJucator.DistantaEvadareCurenta;
        animatie.emitting = true;
        atributeJucator.EsteInvulnerabil = true;
        yield return new WaitForSeconds(durataEvadare);
        atributeJucator.EsteInvulnerabil = false;
        animatie.emitting = false;
        gameObject.layer = 6;
        EsteInEvadare = false;
        StartIncarcareEvadare.Invoke();
        yield return new WaitForSeconds(atributeJucator.TimpReactivareEvadareCurent);
        StopIncarcareEvadare.Invoke();
        PoateActionaEvadare = true;


    }

    public IEnumerator Incetinire()
    {
        var valInitialaViteza = atributeJucator.VitezaMiscareCurenta;

        if(atributeJucator.VitezaMiscareCurenta != vitezaIncetinita)
        {
            StartIncetinire.Invoke();
            atributeJucator.VitezaMiscareCurenta = vitezaIncetinita;
            yield return new WaitForSeconds(durataIncetinire);
            StopIncetinire.Invoke();
            atributeJucator.VitezaMiscareCurenta = valInitialaViteza;
            
        }


    }

    public float ObtineTimpReactivareEvadare()
    {
        return atributeJucator.TimpReactivareEvadareCurent;
    }

    public void PornesteImpingere(Vector2 contact, float vitezaContact)
    {
        StartCoroutine(Impinge(contact, vitezaContact));
    }

    public IEnumerator Impinge(Vector2 contact, float vitezaContact)
    {
        contactImpingere = contact;
        vitezaImpingere = vitezaContact;
        EsteInImpingere = true;
        yield return new WaitForSeconds(durataImpingere);
        EsteInImpingere = false;
    }


}
