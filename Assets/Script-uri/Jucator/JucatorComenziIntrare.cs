using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class JucatorComenziIntrare : MonoBehaviour
{

    private Vector2 vectorRotatie;

    private DeplasareJucator deplasareJucator;
    private AtacJucator atacJucator;

    void Start()
    {
        deplasareJucator = GetComponent<DeplasareJucator>();
        atacJucator = GetComponent<AtacJucator>();
    }

    void Update()
    {
        
    }

    
    public void rotatie()
    {
        var pozitieMouse = Mouse.current.position.ReadValue();
        deplasareJucator.VectorRotatie = Camera.main.ScreenToWorldPoint(pozitieMouse);
        deplasareJucator.Rotatie();
    }

    public void deplasare(InputAction.CallbackContext context)
    {
        deplasareJucator.VectorDeplasare = context.ReadValue<Vector2>();
    }

    public void evadare(InputAction.CallbackContext context)
    {
        if (context.performed && deplasareJucator.PoateActionaEvadare)
        {
            var sincronizare = ManipulatorSunet.instanta.Sincronizare(out string NaturaRezultat);
            if(sincronizare)
            {
                StartCoroutine(deplasareJucator.ActioneazaEvadare());
                atacJucator.ExecutatCuSucces();
            }
            else if (sincronizare == false)
            {
                if (NaturaRezultat.Equals("TARZIU"))
                    atacJucator.Stai(true, false);
                else if (NaturaRezultat.Equals("DEVREME"))
                    atacJucator.Stai(false, true);

                StartCoroutine(deplasareJucator.Incetinire());
            }
        }
           

    }

    public void trage(InputAction.CallbackContext context)
    {
        if (context.started)
        {
            var directieTragere = deplasareJucator.VectorRotatie - (Vector2)transform.position;
            var sincronizare = ManipulatorSunet.instanta.Sincronizare(out string NaturaRezultat);

            if(sincronizare == true)
            {
                atacJucator.Ataca(directieTragere.normalized);
            }
            else if(sincronizare == false)
            {
                if (NaturaRezultat.Equals("TARZIU"))
                    atacJucator.Stai(true, false);
                else if (NaturaRezultat.Equals("DEVREME"))
                    atacJucator.Stai(false, true);

                StartCoroutine(deplasareJucator.Incetinire());
            }
        }

    }


}
