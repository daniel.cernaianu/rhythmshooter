using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AtacJucator : MonoBehaviour
{

    
    [SerializeField] private Transform pozitieGlont;
    [SerializeField] private int combo;
    [SerializeField] LaserPrezentator laserUI;
    private ArmamentBaza armamentEchipat;

    private Jucator atributeBaza;
    private bool atacTarziu;
    private bool atacDevreme;

    public int Combo { get => combo; private set => combo = value; }
    public bool AtacTarziu { get => atacTarziu; private set => atacTarziu = value; }
    public bool AtacDevreme { get => atacDevreme; private set => atacDevreme = value; }
    public ArmamentBaza ArmamentEchipat { get => armamentEchipat; private set => armamentEchipat = value; } 


    public event Action ComboSchimbat;
    public event Action ArmamentModificat;


    void Start()
    {
        
        atributeBaza = GetComponent<Jucator>();
        atributeBaza.AdaugareEchipament += ActualizeazaStatus;
        armamentEchipat = Instantiate(atributeBaza.armamentAlesJucator, transform);
        if(armamentEchipat is ArmamentRazaLaser)
        {
            laserUI.Initializeaza((ArmamentRazaLaser)armamentEchipat);
            laserUI.ActiveazaPanou();
        }
        else
        {
            laserUI.DezactiveazaPanou();
        }

        atributeBaza.Inventar.InitializeazaInventar(armamentEchipat);

        combo = 0;
    }

    public void ActualizeazaStatus()
    {
        
    }

    public void OnDestroy()
    {
        atributeBaza.AdaugareEchipament -= ActualizeazaStatus;
    }

    public void Ataca(Vector2 directie)
    {
        if(armamentEchipat.PoateAtaca)
        {
            ExecutatCuSucces();

            var bonusCombo = CalculeazaBonusDauneCombo();

            CatalogVFX.instanta.RuleazaVFX("blit_armament", pozitieGlont.position, pozitieGlont.rotation, pozitieGlont);

            atributeBaza.AnimatorJucator.SetTrigger("Ataca");
            
            armamentEchipat.Ataca(pozitieGlont, directie, bonusCombo);
        }
    }

    public void ModificaArmamentEchipat(ArmamentBaza armamentNou)
    {
        armamentEchipat = armamentNou;
        if (armamentEchipat is ArmamentRazaLaser)
        {
            laserUI.Initializeaza((ArmamentRazaLaser)armamentEchipat);
            laserUI.ActiveazaPanou();
        }
        else
        {
            laserUI.DezactiveazaPanou();
        }
    }

    public float CalculeazaBonusDauneCombo()
    {
        var dauneBaza = armamentEchipat.PutereDauneCurenta;
        var dauneBonusPerCombo = atributeBaza.BonusPutereDauneComboRitmCurent;

        var dauneBazaCombo = dauneBaza * (dauneBonusPerCombo * combo) / 100f;

        return dauneBazaCombo;
        
    }

    public void Stai(bool atacTarziu, bool atacDevreme)
    {
        combo = 0;
        this.atacTarziu = atacTarziu;
        this.atacDevreme = atacDevreme;
        ActualizeazaUI();
    }

    public void ExecutatCuSucces()
    {
        combo++;
        atacTarziu = false;
        atacDevreme = false;
        ActualizeazaUI();
    }

    

    public void ActualizeazaUI()
    {
        ComboSchimbat.Invoke();
    }
}
