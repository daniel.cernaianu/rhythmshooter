using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;

//Model
public class Jucator : MonoBehaviour
{
    [SerializeField]
    private AtributeScriptableObject atribute;
    
     
    #region Campuri Serializate
    [SerializeField] private ManagerInventar inventar;
    [SerializeField] private JucatorScriptableObject atributeBaza;
    [SerializeField] private float timpDeInvulnerabilitateDupaDaune;
    [SerializeField] private List<IntervalNivel> IntervaleNivel;
    [SerializeField] private float timpOprit;
    [SerializeField] private float durataHitStop;
    #endregion

    #region Proprietati Publice

    public float TimpDeInvulnerabilitateDupaDaune { get => timpDeInvulnerabilitateDupaDaune; set => timpDeInvulnerabilitateDupaDaune = value; }
    public bool EsteInvulnerabil { get; set; } = false;
    public int Nivel { get; set; } = 1;
    public float Experienta { get; set; } = 0f;
    public float LimitaExperienta { get; set; } = 0f;
    public ManagerInventar Inventar { get => inventar; private set => inventar = value; }
    public HitStop hitStop;
    public Animator AnimatorJucator { get; set; }
    #endregion

    #region Campuri Publice
    public float PuncteViataCurenta;
    public float VitezaMiscareCurenta;
    public float DistantaEvadareCurenta;
    public float TimpReactivareEvadareCurent;
    public float DistantaMagnetSteluteXPCurenta;
    public float BonusPutereDauneComboRitmCurent;
    public int PuncteViataMaximaCurenta;

    public event Action SchimbarePuncteViata;
    public event Action SchimbarePuncteExperienta;
    public event Action AdaugareEchipament;
    public event Action AlegereRamuraArmament;
    public event Action<int> CresteNivel;
    public event Action<Vector2, float> IaDaune;
    public event Action Moarte;
    public ArmamentBaza armamentAlesJucator;
    #endregion

    #region DEBUG
    //public SpriteRenderer sprite;
    #endregion

    #region Nivel & Experienta
    [System.Serializable]
    public class IntervalNivel
    {
        public int nivelInceput;
        public int nivelFinal;
        public float sporireLimitaExperienta;
    }

    public void AdaugaExperienta(float experientaPrimita)
    {
        Experienta += experientaPrimita;
        VerificaCrestereaNivelului();
        ActualizeazaPuncteExperientaUI();
    }

    private void VerificaCrestereaNivelului()
    {
        if(Experienta >= LimitaExperienta)
        {
            Nivel++;
            Experienta -= LimitaExperienta;
            float sporireLimitaExperienta = 0f;

            foreach(IntervalNivel interval in IntervaleNivel)
            {
                if(Nivel >= interval.nivelInceput && Nivel <= interval.nivelFinal)
                {
                    sporireLimitaExperienta = interval.sporireLimitaExperienta;
                    break;
                }
            }

            LimitaExperienta += sporireLimitaExperienta;
            CrestereNivel(Nivel);

        }
    }

    #endregion

    #region PuncteViata

    public IEnumerator ReducePuncteViata(float cantitateDeRedus, Vector2 contact, float vitezaContact)
    {
        if (!EsteInvulnerabil)
        {
            PuncteViataCurenta -= cantitateDeRedus;
            hitStop.ModificaTimp(timpOprit, durataHitStop);
            ActualizeazaPuncteViataUI();
            EsteInvulnerabil = true;
            AnimatorJucator.SetBool("Invulnerabil", true);
            CatalogVFX.instanta.RuleazaVFX("lovitura", contact, Quaternion.identity);
            
            IaDaune.Invoke(contact, vitezaContact);
            yield return new WaitForSeconds(timpDeInvulnerabilitateDupaDaune);
            AnimatorJucator.SetBool("Invulnerabil", false);
            EsteInvulnerabil = false;
        }
        if (PuncteViataCurenta <= 0f)
        {
            Moarte.Invoke();
        }
    }

    

    public void RestaureazaPuncteViata(float cantitateDeRestaurat)
    {
        PuncteViataCurenta += cantitateDeRestaurat;
        ActualizeazaPuncteViataUI();
    }

    public void RestaureazaToatePuncteleDeViata()
    {
        PuncteViataCurenta = PuncteViataMaximaCurenta;
        ActualizeazaPuncteViataUI();
    }

    public void AdaugaPuncteViataMaxima(int cantitateAdaugata)
    {
        PuncteViataMaximaCurenta += cantitateAdaugata;
        ActualizeazaPuncteViataUI();
    }

    #endregion

    public void AdaugaEchipamentInInventar(EchipamentBaza echipament)
    {
        inventar.AdaugaEchipament(echipament);
    }

    public void Start()
    {
        AnimatorJucator = GetComponentInChildren<Animator>();
        hitStop = GetComponent<HitStop>();
        inventar.AdaugareEchipament += PeAdaugareaEchipamentului;

        LimitaExperienta = IntervaleNivel[0].sporireLimitaExperienta;

        ActualizeazaPuncteExperientaUI();
        ActualizeazaPuncteViataUI();
    }

    public void PeAdaugareaEchipamentului(EchipamentBaza echipament)
    {
        Instantiate(echipament, transform);
    }

    public void OnDestroy()
    {
        inventar.AdaugareEchipament -= PeAdaugareaEchipamentului;
    }

    public void Awake()
    {
        if(armamentAlesJucator == null)
        {
            armamentAlesJucator = SelectorArmament.ObtineArmamentAles();
            
            SelectorArmament.instanta.DestroySingleton();
           // inventar.InitializeazaInventar(armamentAlesJucator);
        }

        PuncteViataCurenta = atributeBaza.PuncteViataMaxime;

        PuncteViataMaximaCurenta = atributeBaza.PuncteViataMaxime;

        VitezaMiscareCurenta = atributeBaza.VitezaMiscare;

        DistantaEvadareCurenta = atributeBaza.DistantaEvadare;

        TimpReactivareEvadareCurent = atributeBaza.TimpReactivareEvadare;

        DistantaMagnetSteluteXPCurenta = atributeBaza.DistantaMagnetSteluteXP;

        BonusPutereDauneComboRitmCurent = atributeBaza.BonusPutereDauneComboRitm;
        
    }

    public void Update()
    {
        
    }


    

    #region Actualizari UI
    public void ActualizeazaPuncteViataUI()
    {
        SchimbarePuncteViata.Invoke();      
    }

    public void ActualizeazaPuncteExperientaUI()
    {
        SchimbarePuncteExperienta.Invoke();
    }

    public void CrestereNivel(int nivel)
    {
        CresteNivel.Invoke(nivel);
    }

    #endregion

    

}
