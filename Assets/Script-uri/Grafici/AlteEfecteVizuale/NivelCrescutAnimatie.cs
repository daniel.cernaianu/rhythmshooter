using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NivelCrescutAnimatie : MonoBehaviour
{
    #region -- CAMPURI --

    #region -- PRIVATE --

    #region -- SERIALIZATE --
    
    [SerializeField]
    private TextNivelCrescut nivelCrescutTMP;
    
    #endregion

    #endregion

    #endregion

    #region -- SINGLETON --

    public static NivelCrescutAnimatie Instanta { get; private set; }

    #endregion

    #region -- METODE --

    #region -- PRIVATE --

    private void Awake()
    {
        if (Instanta == null)
            Instanta = this;
    }

    #endregion

    #region -- PUBLICE --

    public IEnumerator Ruleaza(float durata, Transform pozitieInstantiere, Action callback)
    {
        var animatie = Instantiate(nivelCrescutTMP, pozitieInstantiere);

        yield return new WaitForSeconds(durata);

        animatie.Distruge();

        callback.Invoke();
    }

    #endregion

    #endregion
}
