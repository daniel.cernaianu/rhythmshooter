using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitStop : MonoBehaviour
{
    private bool impactInDerulare = false; 

    public void ModificaTimp(float valoareTimp, float durata)
    {
        if (!impactInDerulare)
        {
            
            Time.timeScale = valoareTimp;
            StartCoroutine(Asteapta(durata));
        }
    }

    public IEnumerator Asteapta(float durata)
    {
        impactInDerulare = true;
        yield return new WaitForSecondsRealtime(durata);
        Time.timeScale = 1f;
        impactInDerulare = false;
    }

}
