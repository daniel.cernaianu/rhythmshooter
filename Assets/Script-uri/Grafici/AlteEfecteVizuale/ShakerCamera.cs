using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShakerCamera : MonoBehaviour
{

    //Script responsabil pentru efectul de shake al camerei.
    //Script atasat de camera virtuala. 

    #region -- CAMPURI --

    #region -- PRIVATE --

    #region -- COMPONENTS -- 

    private CinemachineVirtualCamera cameraVirtuala;
    private CinemachineBasicMultiChannelPerlin zgomotPerlin;

    #endregion

    private float shakeTimer;

    #endregion

    #endregion

    #region-- SINGLETON --
    public static ShakerCamera Instanta { get; private set; }
    #endregion

    #region -- METODE --

    #region -- PRIVATE --

    private void Awake()
    {
        if (Instanta == null)
        {
            Instanta = this;
        }
        cameraVirtuala = GetComponent<CinemachineVirtualCamera>();
        zgomotPerlin = cameraVirtuala.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
    }
    private void Update()
    {
        if (shakeTimer > 0f)
        {
            shakeTimer -= Time.deltaTime;
            if (shakeTimer <= 0f)
            {
                zgomotPerlin.m_AmplitudeGain = 0f;
            }
        }
    }

    #endregion
    #region -- PUBLICE --

    public void ShakeCamera(float intensitate, float durata)
    {
        zgomotPerlin.m_AmplitudeGain = intensitate;
        shakeTimer = durata;
    }

    #endregion

    #endregion




}
