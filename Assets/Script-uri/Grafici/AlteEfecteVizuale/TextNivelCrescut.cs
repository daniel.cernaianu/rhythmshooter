using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;

public class TextNivelCrescut : MonoBehaviour
{
    //Script responsabil pentru animatia de crestere nivel.
    //Atasat obiectului prefab TMP_Text NivelCrescutTMP.

    #region -- CAMPURI --

    #region -- PRIVATE --

    #region -- SERIALIZATE --
    [SerializeField]
    private float vitezaMiscare;
    [SerializeField]
    private float distantaMiscare;
    [SerializeField]
    private Vector3 directieMiscare;

    #endregion

    #endregion

    #endregion

    #region -- METODE --

    #region -- PRIVATE --

    private void Update()
    {
        RuleazaAnimatie();
    }

    #endregion

    #region -- PUBLICE --

    public void RuleazaAnimatie()
    {
        transform.DOLocalMove(directieMiscare * distantaMiscare, vitezaMiscare);
    }

    public void Distruge()
    {
        Destroy(gameObject);
    }
    #endregion
    #endregion

    



}
