using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatalogVFX : MonoBehaviour
{

    #region -- CAMPURI --

    #region -- PRIVATE --

    #region -- SERIALIZATE --

    [SerializeField] private List<VFXScriptableObject> catalog;

    #endregion

    #endregion

    #region -- PUBLICE --

    public static CatalogVFX instanta;

    #endregion

    #endregion

    public void Awake()
    {
        if (instanta == null)
        {
            instanta = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void DestroySingleton()
    {
        instanta = null;
        Destroy(gameObject);
    }

    public void RuleazaVFX(string nume, Vector2 pozitie, Quaternion rotatie, float dimensiune = 0f)
    {
        var vfxDorit = catalog.Find(vfx => vfx.Nume.Equals(nume));

        var vfxInstantiat = Instantiate(vfxDorit.efect, pozitie, rotatie);

        if (vfxInstantiat.HasFloat("Dimensiune"))
        {
            vfxInstantiat.SetFloat("Dimensiune", dimensiune);
        }

        vfxInstantiat.Play();
        Destroy(vfxInstantiat.gameObject, vfxDorit.durata);
    }

    public void RuleazaVFX(string nume, Vector2 pozitie, Quaternion rotatie, Transform parinte, float dimensiune = 0f)
    {
        var vfxDorit = catalog.Find(vfx => vfx.Nume.Equals(nume));

        var vfxInstantiat = Instantiate(vfxDorit.efect, pozitie, rotatie, parinte);

        if (vfxInstantiat.HasFloat("Dimensiune"))
        {
            vfxInstantiat.SetFloat("Dimensiune", dimensiune);
        }

        vfxInstantiat.Play();
        Destroy(vfxInstantiat.gameObject, vfxDorit.durata);
    }
}
