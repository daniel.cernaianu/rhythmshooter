using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class TextRezultatRitm : MonoBehaviour
{
    //Script pentru prefab-ul de rezultat pentru actiuni pe ritm.

    #region -- CAMPURI --

    #region -- PRIVATE --

    #region -- SERIALIZATE --

    [SerializeField]
    private float vitezaMiscare;

    [SerializeField]
    private float distantaMiscare;

    [SerializeField]
    private Vector3 directieMiscare;

    [SerializeField]
    private float timpDisparitie;

    [SerializeField]
    private float valoareAlpha;

    #endregion

    #region -- COMPONENTE --

    private TMP_Text text;

    #endregion

    #endregion

    #endregion

    #region -- METODE --

    #region -- PRIVATE --

    private void Start()
    {
        text = GetComponent<TMP_Text>();
    }

    private void Awake()
    {
        StartCoroutine(AfiseazaText());
    }

    #endregion

    #region -- PUBLICE --

    public IEnumerator AfiseazaText()
    {
        Sequence secventa = DOTween.Sequence();
        secventa.Append(transform.DOLocalMove(directieMiscare * distantaMiscare, vitezaMiscare));
        secventa.Append(text.DOFade(valoareAlpha, timpDisparitie));
        yield return new WaitForSeconds(vitezaMiscare + timpDisparitie);
        Destroy(this.gameObject);

    }

    #endregion

    #endregion

}
