using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PanouIconita : MonoBehaviour
{

    [SerializeField]
    private Sprite iconita;

    public Sprite Iconita { get => iconita; set => iconita = value; }

    [SerializeField]
    private TMP_Text nivel;
    
    public string Nivel { get; set; }


    public void SeteazaImagineNivel()
    {
        var imagine = GetComponent<Image>();
        imagine.sprite = iconita;
        nivel.text = Nivel;
    }

    public void SeteazaNivel()
    {
        nivel.text = Nivel;
    }

}
