using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PanouEchipament : MonoBehaviour
{
    [SerializeField]
    private EchipamentBaza echipament;
    public EchipamentBaza Echipament { get => echipament; private set => echipament = value; }

    [SerializeField]
    private Image iconita;
    public Image Iconita { get => iconita; private set => iconita = value; }
    [SerializeField]
    private TMP_Text nume;
    public TMP_Text Nume { get => nume; private set => nume = value; }
    [SerializeField]
    private TMP_Text descriere;
    public TMP_Text Descriere { get => descriere; private set => descriere = value; }
    [SerializeField]
    private TMP_Text nivel;
    public TMP_Text Nivel { get => nivel; private set => nivel = value; }


    public Outline efectBordura;

    public event Action<PanouEchipament> PeAlegere;
    public bool EsteAles { get; set; } = false;

    public void PeClic()
    {
        EsteAles = !EsteAles;
        efectBordura.enabled = !efectBordura.enabled;
        PeAlegere.Invoke(this);
    }

    public int NivelEchipament()
    {
        return Echipament.echipamentAtributeBaza.Nivel;
    }

    public string NumeEchipament()
    {
        return Echipament.echipamentAtributeBaza.Nume;
    }

    public void Awake()
    {

        efectBordura = GetComponent<Outline>();
        iconita.sprite = echipament.echipamentAtributeBaza.Iconita;
        nume.text = echipament.echipamentAtributeBaza.Nume;
        descriere.text = echipament.echipamentAtributeBaza.Descriere;
        nivel.text = "Nivel: " + (echipament.NivelCurent);
         
    }


}
