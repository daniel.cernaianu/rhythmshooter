using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PanouArmament : MonoBehaviour
{

    [SerializeField]
    private ArmamentBaza armament;
    public ArmamentBaza Armament { get => armament; private set => armament = value; }

    [SerializeField]
    private Image iconita;
    public Image Iconita { get => iconita; private set => iconita = value; }
    [SerializeField]
    private TMP_Text nume;
    public TMP_Text Nume { get => nume; private set => nume = value; }
    [SerializeField]
    private TMP_Text descriere;
    public TMP_Text Descriere { get => descriere; private set => descriere = value; }

    public Outline efectBordura;

    public event Action<PanouArmament> PeAlegere;
    public bool EsteAles { get; set; } = false;

    public void PeClic()
    {
        EsteAles = !EsteAles;
        efectBordura.enabled = !efectBordura.enabled;
        PeAlegere.Invoke(this);
    }

    public void Awake()
    {
        efectBordura = GetComponent<Outline>();
        
        iconita.sprite = armament.dateArmament.Iconita;
        nume.text = armament.dateArmament.Nume;
        descriere.text = armament.dateArmament.Descriere;
    }

    
}
