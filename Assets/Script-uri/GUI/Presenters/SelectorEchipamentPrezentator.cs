using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class SelectorEchipamentPrezentator : MonoBehaviour
{

    [Header("Model")]

    [SerializeField]
    private SelectorEchipament modelSelector;

    [Header("View")]

    [SerializeField]
    private GameObject SelectorEchipamentUI;

    [SerializeField]
    private GameObject ButonConfirmare;

    public PanouEchipament panouAles;
    private List<PanouEchipament> panouriInstantiate = new List<PanouEchipament>();



    public void Start()
    {
        modelSelector.AlegereEchipament += AfiseazaSelector;
    }

    public void OnDestroy()
    {
        modelSelector.AlegereEchipament -= AfiseazaSelector;
    }

    public void AfiseazaSelector(bool specificArmament)
    {

        SelectorEchipamentUI.transform.parent.gameObject.SetActive(true);
        SelectorEchipamentUI.SetActive(true);

        if (specificArmament)
        {
            foreach (PanouEchipament panou in modelSelector.Inventar.ObtineArmament().dateArmament.PanouriSpecifice)
            {
                var panouInstantiat = Instantiate(panou, SelectorEchipamentUI.transform);
                panouriInstantiate.Add(panouInstantiat);
            }

        }
        else
        {
            foreach (PanouEchipament panou in modelSelector.EchipamenteDeAles)
            {
                var panouInstantiat = Instantiate(panou, SelectorEchipamentUI.transform);
                panouriInstantiate.Add(panouInstantiat);
            }
        }
        foreach (PanouEchipament panou in panouriInstantiate)
        {
            panou.PeAlegere += AlegePanou;
        }
    }

    public void AlegePanou(PanouEchipament panou)
    {
        if (panou.EsteAles)
        {
            SeteazaRestulPanourilor(panou, false);
            ButonConfirmare.SetActive(true);
            panouAles = panou;

        }
        else if (!panou.EsteAles)
        {
            SeteazaRestulPanourilor(panou, true);
            ButonConfirmare.SetActive(false);
            panouAles = null;
        }
    }

    public void SeteazaRestulPanourilor(PanouEchipament panouExclus, bool valoare)
    {
        var panouriNeAlese = panouriInstantiate.Except(new List<PanouEchipament> { panouExclus });

        foreach (PanouEchipament p in panouriNeAlese)
        {
            p.gameObject.SetActive(valoare);
        }
    }

    public void Confirmare()
    {
        modelSelector.panouAles = panouAles;
        modelSelector.EchipeazaEchipamentulAles();
        modelSelector.ObtineEchipamenteArmament(panouAles);
        modelSelector.EliminaEchipamenteNepotrivite();
        modelSelector.ActualizeazaNiveluriEchipamentePosibile();
        modelSelector.FormeazaEchipamentePosibile();
        modelSelector.EsteActivat = false;

        ButonConfirmare.SetActive(false);
        SeteazaRestulPanourilor(panouAles, true);
        EliminaPanouri();

        SelectorEchipamentUI.transform.parent.gameObject.SetActive(false);

    }

    public void EliminaPanouri()
    {
        foreach (PanouEchipament echipament in SelectorEchipamentUI.GetComponentsInChildren<PanouEchipament>())
        {
            echipament.PeAlegere -= AlegePanou;
            Destroy(echipament.gameObject);
        }
        panouriInstantiate.Clear();
    }

    

    public void InstantiazaPanouri()
    {

    }

    
}
