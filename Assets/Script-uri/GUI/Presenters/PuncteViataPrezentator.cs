using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PuncteViataPrezentator : MonoBehaviour
{
    [Header("Model")]
    [SerializeField] private Jucator modelJucator;
    [Header("View")]
    [SerializeField] private Image puncteViata;

    void Start()
    {
        modelJucator.SchimbarePuncteViata += PeSchimbareaPunctelorDeViata;
    }

    private void PeSchimbareaPunctelorDeViata() 
    {
        ActualizeazaUIPuncteDeViata();
    }

    private void ActualizeazaUIPuncteDeViata()
    {
        puncteViata.fillAmount = modelJucator.PuncteViataCurenta/modelJucator.PuncteViataMaximaCurenta;
    }


    void Update()
    {
        
    }

    private void OnDestroy()
    {

        if(modelJucator != null)
        {
            modelJucator.SchimbarePuncteViata -= PeSchimbareaPunctelorDeViata;
        }
        
    }
}
