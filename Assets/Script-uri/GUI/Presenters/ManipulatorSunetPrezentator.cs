using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;


//Presenter pentru ManipulatorSunet(Model)
public class ManipulatorSunetPrezentator : MonoBehaviour
{
    public Sequence secventaAnimatie;
    [Header("View")]
    [SerializeField] private RectTransform perecheBataiRectTransform;
    [SerializeField] private RectTransform centruBataiRectTransform;

    void Start()
    {
        ManipulatorSunet.instanta.BataieSuccesata += PeBataieSuccesata;
    }

    public void Ruleaza()
    {
        secventaAnimatie = DOTween.Sequence();
        secventaAnimatie.Append(perecheBataiRectTransform.DOScale(0f, 0.1f));
        secventaAnimatie.Append(centruBataiRectTransform.DOScale(1.2f, 0.1f));
        secventaAnimatie.Append(centruBataiRectTransform.DOScale(1f, 0f));
        secventaAnimatie.Append(perecheBataiRectTransform.DOScale(1f, 0f));
    }

    public void PeBataieSuccesata()
    {
        Ruleaza();
    }

    private void OnDestroy()
    {
        ManipulatorSunet.instanta.BataieSuccesata -= PeBataieSuccesata;
    }

}
