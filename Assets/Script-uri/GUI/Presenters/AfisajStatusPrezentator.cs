using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class AfisajStatusPrezentator : MonoBehaviour, IVisitor
{

    [Header("Model")]
    [SerializeField]
    private AfisajStatus modelAfisajStatus;

    #region Campuri Serializate
    [Header("Nava Viewer")]
    [SerializeField]
    private TMP_Text puncteViataText;
    [SerializeField]
    private TMP_Text vitezaMiscareText;
    [SerializeField]
    private TMP_Text distantaEvadareText;
    [SerializeField]
    private TMP_Text reactivareEvadareText;
    [SerializeField]
    private TMP_Text magnetismText;
    [SerializeField]
    private TMP_Text experientaText;
    [SerializeField]
    private TMP_Text nivelText;
    [SerializeField]
    private TMP_Text putereRitmText;
    [Header("Armament Viewer")]
    [SerializeField]
    private TMP_Text dauneText;
    [SerializeField]
    private TMP_Text distProiectileText;
    [SerializeField]
    private TMP_Text vitezaProiectileText;
    [SerializeField]
    private TMP_Text nrProiectileText;
    [SerializeField]
    private TMP_Text strapungereText;
    [SerializeField]
    private TMP_Text rezistentaText;
    [SerializeField]
    private TMP_Text razaExplozieText;
    [SerializeField]
    private TMP_Text dauneExplozieText;
    #endregion

    private string tiparPuncteViata;
    private string tiparVitezaMiscare;
    private string tiparDistantaEvadare;
    private string tiparReactivareEvadare;
    private string tiparExperienta;
    private string tiparNivel;
    private string tiparPutereRitm;
    private string tiparMagnetism;
    private string tiparDaune;
    private string tiparVitezaProiectil;
    private string tiparDistantaProiectil;
    private string tiparNrProiectile;
    private string tiparStrapungere;
    private string tiparRazaExplozie;
    private string tiparDauneExplozie;
    private string tiparRezistenta;



    // Start is called before the first frame update
    void Start()
    {
        modelAfisajStatus.ActualizareAfisaj += Actualizeaza;

        tiparPuncteViata = puncteViataText.text;
        tiparVitezaMiscare = vitezaMiscareText.text;
        tiparDistantaEvadare = distantaEvadareText.text;
        tiparReactivareEvadare = reactivareEvadareText.text;
        tiparExperienta = experientaText.text;
        tiparNivel = nivelText.text;
        tiparPutereRitm = putereRitmText.text;
        tiparMagnetism = magnetismText.text;
        tiparDaune = dauneText.text;
        tiparVitezaProiectil = vitezaProiectileText.text;
        tiparDistantaProiectil = distProiectileText.text;
        tiparStrapungere = strapungereText.text;
        tiparNrProiectile = nrProiectileText.text;
        tiparRazaExplozie = razaExplozieText.text;
        tiparDauneExplozie = dauneExplozieText.text;
        tiparRezistenta = rezistentaText.text;

    }


    private void OnDestroy()
    {
        modelAfisajStatus.ActualizareAfisaj -= Actualizeaza;
    }

    public void Actualizeaza()
    {
        ActualizeazaInfoNava();
        ActualizeazaInfoArmament();
    }

    public void ActualizeazaInfoNava()
    {
        puncteViataText.text = tiparPuncteViata + $" {modelAfisajStatus.JucatorCurent.PuncteViataCurenta}/{modelAfisajStatus.JucatorCurent.PuncteViataMaximaCurenta}";

        vitezaMiscareText.text = tiparVitezaMiscare + $" {modelAfisajStatus.JucatorCurent.VitezaMiscareCurenta}";

        distantaEvadareText.text = tiparDistantaEvadare + $" {modelAfisajStatus.JucatorCurent.DistantaEvadareCurenta}";

        reactivareEvadareText.text = tiparReactivareEvadare + $" {modelAfisajStatus.JucatorCurent.TimpReactivareEvadareCurent}s";

        magnetismText.text = tiparMagnetism + $" {modelAfisajStatus.JucatorCurent.DistantaMagnetSteluteXPCurenta}";

        experientaText.text = tiparExperienta + $" {modelAfisajStatus.JucatorCurent.Experienta}/{modelAfisajStatus.JucatorCurent.LimitaExperienta}";

        nivelText.text = tiparNivel + $" {modelAfisajStatus.JucatorCurent.Nivel}";

        putereRitmText.text = tiparPutereRitm + $" {modelAfisajStatus.JucatorCurent.BonusPutereDauneComboRitmCurent}%";

    }

    public void ActualizeazaInfoArmament()
    {
        var armament = modelAfisajStatus.AtacJucatorCurent.ArmamentEchipat;

        armament.Accepta(this);
    }

    public void Visit(ArmamentGlontPlasma obj)
    {
        vitezaProiectileText.text = tiparVitezaProiectil + $" {obj.VitezaGlontCurenta}";

        distProiectileText.text = tiparDistantaProiectil + $" {obj.DistantaGlontCurenta}";

        nrProiectileText.text = tiparNrProiectile + $" {obj.NumarProiectileCurent}";

        strapungereText.text = tiparStrapungere + $" {obj.Strapungere}";

        razaExplozieText.gameObject.SetActive(false);
        dauneExplozieText.gameObject.SetActive(false);
        rezistentaText.gameObject.SetActive(false);
    }

    public void Visit(ArmamentRazaLaser obj)
    {
        vitezaProiectileText.text = tiparVitezaProiectil + $" {obj.VitezaGlontCurenta}(Convertit la Daune)";

        distProiectileText.text = tiparDistantaProiectil + $" {obj.DistantaGlontCurenta}(Convertit la Rez. Laser)";

        nrProiectileText.text = tiparNrProiectile + $" {obj.NumarProiectileCurent}";

        rezistentaText.text = tiparRezistenta + $" {obj.RezistentaLaser}s";

        razaExplozieText.gameObject.SetActive(false);
        dauneExplozieText.gameObject.SetActive(false);
        strapungereText.gameObject.SetActive(false);
    }

    public void Visit(ArmamentAruncatorRachete obj)
    {
        vitezaProiectileText.text = tiparVitezaProiectil + $" {obj.VitezaGlontCurenta}";

        distProiectileText.text = tiparDistantaProiectil + $" {obj.DistantaGlontCurenta}";

        nrProiectileText.text = tiparNrProiectile + $" {obj.NumarProiectileCurent}";

        razaExplozieText.text = tiparRazaExplozie + $" {obj.RazaExplozie}";

        dauneExplozieText.text = tiparDauneExplozie + $" {obj.DauneExplozie}";

        strapungereText.gameObject.SetActive(false);
        rezistentaText.gameObject.SetActive(false);
    }

    public void Visit(ArmamentBaza obj)
    {
        dauneText.text = tiparDaune + $" {obj.PutereDauneCurenta}";
    }


}
