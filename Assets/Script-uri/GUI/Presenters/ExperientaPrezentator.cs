using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ExperientaPrezentator : MonoBehaviour
{
    [Header("Model")]
    [SerializeField] private Jucator modelJucator;

    [Header("View")]
    [SerializeField] private Image experienta;
    [SerializeField] private TMP_Text nivelText;
    [SerializeField] private TMP_Text experientaText;

    private void Start()
    {
        modelJucator.SchimbarePuncteExperienta += PeSchimbareaPunctelorExperienta;
    }

    private void PeSchimbareaPunctelorExperienta()
    {
        ActualizeazaUIExperienta();
    }

    private void ActualizeazaUIExperienta()
    {
        experienta.fillAmount = modelJucator.Experienta / modelJucator.LimitaExperienta;
        experientaText.text = String.Format("{0}/{1}", modelJucator.Experienta, modelJucator.LimitaExperienta);
        nivelText.text = String.Format("{0}", modelJucator.Nivel);
        
    }

    private void OnDestroy()
    {
        if(modelJucator != null)
        {
            modelJucator.SchimbarePuncteExperienta -= PeSchimbareaPunctelorExperienta;
        }
    }
}
