using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EvadarePrezentator : MonoBehaviour
{

    [Header("Model")]
    [SerializeField] private DeplasareJucator deplasareJucator;

    private float durataRamasa;
    private bool evadareInIncarcare;

    [Header("View")]
    [SerializeField] private Image imagineEvadare;


    public void Start()
    {
        deplasareJucator.StartIncarcareEvadare += IncarcareEvadare;
        deplasareJucator.StopIncarcareEvadare += EvadareIncarcata;
    }

    private void Update()
    {
        if (durataRamasa >= 0 && evadareInIncarcare)
        {
            imagineEvadare.fillAmount = durataRamasa / deplasareJucator.ObtineTimpReactivareEvadare();
            durataRamasa -= Time.deltaTime;
        }
    }


    private void EvadareIncarcata()
    {
        

        evadareInIncarcare = false;
    }

    private void IncarcareEvadare()
    {
        durataRamasa = deplasareJucator.ObtineTimpReactivareEvadare();
        imagineEvadare.fillAmount = 1f;
        evadareInIncarcare = true;
    }

    public void OnDestroy()
    {
        deplasareJucator.StartIncarcareEvadare -= IncarcareEvadare;
        deplasareJucator.StopIncarcareEvadare -= EvadareIncarcata;
    }

}
