using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CronometruPrezentator : MonoBehaviour
{

    [Header("Model")]
    [SerializeField] private Cronometru modelCronometru;

    [Header("View")]
    [SerializeField] private TMP_Text timpCurentText;

    // Start is called before the first frame update
    void Start()
    {
        modelCronometru.SchimbareTimp += PeSchimbareaTimpului;
    }

    private void PeSchimbareaTimpului()
    {
        ActualizeazaUITimp();
    }

    private void ActualizeazaUITimp()
    {
        timpCurentText.text = modelCronometru.CalculeazaTimpCurent();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnDestroy()
    {

        if(modelCronometru != null)
        {
            modelCronometru.SchimbareTimp -= PeSchimbareaTimpului;
        }
        
    }
}
