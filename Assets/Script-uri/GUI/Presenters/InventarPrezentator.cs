using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class InventarPrezentator : MonoBehaviour
{


    [Header("Model")]
    [SerializeField] private ManagerInventar modelInventar;

    [Header("View")]
    [SerializeField] private GameObject panouElementeInventar;
    [SerializeField] private GameObject panouArmament;
    [SerializeField] private PanouIconita modelPanouIconita;
    [SerializeField] private TMP_Text textDimensiuneMaxima;

    private List<PanouIconita> panouriGenerale = new List<PanouIconita>();
    private PanouIconita panouSpecificArmament;
    


    public void AdaugarePanouGeneral(EchipamentBaza echipament)
    {
        var sprite = echipament.echipamentAtributeBaza.Iconita;
        var nivel = echipament.NivelCurent;

        var panouIconita = Instantiate(modelPanouIconita, panouElementeInventar.transform);
        panouIconita.Iconita = sprite;
        panouIconita.Nivel = nivel  + "";
        panouIconita.SeteazaImagineNivel();

        panouriGenerale.Add(panouIconita);

    }

    public void AdaugarePanouSpecificArmament(EchipamentBaza echipament)
    {
        var sprite = echipament.echipamentAtributeBaza.Iconita;
        var nivel = echipament.NivelCurent;

        var panouIconita = Instantiate(modelPanouIconita, panouArmament.transform);
        panouIconita.Iconita = sprite;
        panouIconita.Nivel = nivel + "";
        panouIconita.SeteazaImagineNivel();

        panouSpecificArmament = panouIconita;
    }

    private void ActualizeazaEchipamentGeneralInInventar(int pozitie)
    {
        var nivelInt = int.Parse(panouriGenerale[pozitie].Nivel);
        nivelInt++;
        panouriGenerale[pozitie].Nivel = nivelInt + "";
        panouriGenerale[pozitie].SeteazaNivel();
    }

    private void ActualizeazaEchipamentSpecificArmament()
    {
        var nivelInt = int.Parse(panouSpecificArmament.Nivel);
        nivelInt++;
        panouSpecificArmament.Nivel = nivelInt + "";
        panouSpecificArmament.SeteazaNivel();
    }

    // Start is called before the first frame update
    void Start()
    {
        modelInventar.AdaugareEchipamentGeneralInInventar += AdaugarePanouGeneral;
        modelInventar.AdaugareEchipamentSpecificArmament += AdaugarePanouSpecificArmament;
        modelInventar.ActualizeazaEchipamentSpecificArmament += ActualizeazaEchipamentSpecificArmament;
        modelInventar.ActualizeazaEchipamentGeneralInInventar += ActualizeazaEchipamentGeneralInInventar;
        modelInventar.Initializare += PeInitializare;
    }

    private void PeInitializare()
    {
        textDimensiuneMaxima.text += " " + modelInventar.DimensiuneInventar;
    }

    private void OnDestroy()
    {
        modelInventar.AdaugareEchipamentGeneralInInventar -= AdaugarePanouGeneral;
        modelInventar.AdaugareEchipamentSpecificArmament -= AdaugarePanouSpecificArmament;
        modelInventar.ActualizeazaEchipamentSpecificArmament -= ActualizeazaEchipamentSpecificArmament;
        modelInventar.ActualizeazaEchipamentGeneralInInventar -= ActualizeazaEchipamentGeneralInInventar;
        modelInventar.Initializare -= PeInitializare;
    }

    

    // Update is called once per frame
    void Update()
    {
        
    }




}
