using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using DG.Tweening;


public class ComboPrezentator : MonoBehaviour
{
    //Script presenter(intermediar intre view si model) pentru functionalitatea de Combo
    //Script atasat la ManipulatorSunetUI

    #region -- CAMPURI --
    #region -- PRIVATE --
    #region -- SERIALIZATE --
    [Header("Others")]
    [SerializeField]
    private float DurataEfectShake;
    [SerializeField]
    private float FortaEfectShake;
    [SerializeField]
    private float DurataEfectPunch;
    

    [Header("Model")]
    [SerializeField] private AtacJucator atacJucator;

    [Header("View")]
    [SerializeField] private TMP_Text textCombo;
    [SerializeField] private GameObject panouCombo;

    [SerializeField] private Transform pozitieInstantiere;
    [SerializeField] private TextRezultatRitm textAtacSucces;
    [SerializeField] private TextRezultatRitm textAtacTarziu;
    [SerializeField] private TextRezultatRitm textAtacDevreme;
    #endregion
    #endregion
    #endregion

    #region -- METODE --

    #region -- PRIVATE --

    private void Start()
    {
        atacJucator.ComboSchimbat += PeComboSchimbat;
    }
    private void OnDestroy()
    {
        if (atacJucator != null)
            atacJucator.ComboSchimbat -= PeComboSchimbat;
    }

    #endregion

    #region -- PUBLICE --

    public void PeComboSchimbat()
    {
        textCombo.text = string.Format("combo x {0}", atacJucator.Combo);

        panouCombo.transform.DOPunchScale(Vector3.one * atacJucator.Combo / 100f, DurataEfectPunch);

        if (atacJucator.AtacTarziu)
        {
            Instantiate(textAtacTarziu, pozitieInstantiere);

            panouCombo.transform.DOShakePosition(DurataEfectShake);

            ShakerCamera.Instanta.ShakeCamera(FortaEfectShake, DurataEfectShake);

        }
        else if (atacJucator.AtacDevreme)
        {
            Instantiate(textAtacDevreme, pozitieInstantiere);
        }
        else
        {
            Instantiate(textAtacSucces, pozitieInstantiere);
        }
    }

    #endregion

    #endregion









}
