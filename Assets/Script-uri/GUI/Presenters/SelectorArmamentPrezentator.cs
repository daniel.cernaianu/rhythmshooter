using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class SelectorArmamentPrezentator : MonoBehaviour
{

    public PanouArmament[] armamentePosibile;

    public GameObject panouArmamente;

    private List<PanouArmament> panouriInstantiate = new List<PanouArmament>();

    [SerializeField] private Button butonConfirmare;

    public void AlegeArmament(PanouArmament panou)
    {
        if(panou.EsteAles == true)
        {
            SeteazaRestulPanourilor(panou, false);
            SelectorArmament.instanta.armamentAles = panou.Armament;
            butonConfirmare.gameObject.SetActive(true);
        }
        else if (panou.EsteAles == false)
        {
            SeteazaRestulPanourilor(panou, true);
            SelectorArmament.instanta.armamentAles = null;
            butonConfirmare.gameObject.SetActive(false);
        }
    }

    public void SeteazaRestulPanourilor(PanouArmament panouExclus, bool valoare)
    {
        var panouriNeAlese = panouriInstantiate.Except(new List<PanouArmament> { panouExclus });

        foreach (PanouArmament p in panouriNeAlese)
        {
            p.gameObject.SetActive(valoare);
        }
    }


    // Start is called before the first frame update
    void Start()
    {
       foreach(PanouArmament armament in armamentePosibile)
       {
            var panou = Instantiate(armament, panouArmamente.transform);
            panouriInstantiate.Add(panou);
            panou.PeAlegere += AlegeArmament;
       }
    }

    private void OnDestroy()
    {
        foreach(PanouArmament panou in panouriInstantiate)
        {
            if(panou != null)
            {
                panou.PeAlegere -= AlegeArmament;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
