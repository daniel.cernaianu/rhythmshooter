using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using System;

public class LaserPrezentator : MonoBehaviour
{

    [Header("Model")]
    
    private ArmamentRazaLaser laser;

    [Header("View")]

    [SerializeField]
    private Image elementUI;

    [SerializeField]
    private GameObject panouEtape;


    public void Initializeaza(ArmamentRazaLaser Laser)
    {
        laser = Laser;
        laser.ActualizeazaUIEtape += AdaugaEtapaIncarcare;
        laser.ReseteazaEtape += ReseteazaEtapeIncarcare;
        panouEtape.SetActive(false);
    }

    private void ReseteazaEtapeIncarcare()
    {
        elementUI.fillAmount = 0f;
    }

    public void ActiveazaPanou()
    {
        panouEtape.SetActive(true);
    }
    public void DezactiveazaPanou()
    {
        panouEtape.SetActive(false);
    }

    // Start is called before the first frame update
    void Start()
    {
            
    }

    private void OnDestroy()
    {
        if (laser != null)
        {
            laser.ActualizeazaUIEtape -= AdaugaEtapaIncarcare;
            laser.ReseteazaEtape -= ReseteazaEtapeIncarcare;
        }
    }

    public void AdaugaEtapaIncarcare()
    {
        elementUI.fillAmount += 1f/laser.EtapeIncarcare;
    }

    
}
