using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IncetinirePrezentator : MonoBehaviour
{
    [Header("Model")]
    [SerializeField] private DeplasareJucator deplasareJucator;

    private float DurataIncetinireRamasa;
    private bool incetinire = false;

    [Header("View")]
    [SerializeField] private Image imagineIncetinire;

    private void Start()
    {
        deplasareJucator.StartIncetinire += ActiveazaImagine;
        deplasareJucator.StopIncetinire += DezactiveazaImagine;
    }

    private void OnDestroy()
    {
        deplasareJucator.StartIncetinire -= ActiveazaImagine;
        deplasareJucator.StopIncetinire -= DezactiveazaImagine;
    }

    private void DezactiveazaImagine()
    {
        imagineIncetinire.gameObject.SetActive(false);
        imagineIncetinire.fillAmount = 1f;
        incetinire = false;
    }

    public void Update()
    {
        if(DurataIncetinireRamasa >= 0 && incetinire)
        {
            imagineIncetinire.fillAmount = DurataIncetinireRamasa / deplasareJucator.DurataIncetinire;
            DurataIncetinireRamasa -= Time.deltaTime;

        }
        
    }

    private void ActiveazaImagine()
    {
        DurataIncetinireRamasa = deplasareJucator.DurataIncetinire;
        imagineIncetinire.gameObject.SetActive(true);
        incetinire = true;
        
    }

    public IEnumerator PornesteCronometruIncetinire()
    {
        while(DurataIncetinireRamasa >=0)
        {
            imagineIncetinire.fillAmount = Mathf.InverseLerp(0f, deplasareJucator.DurataIncetinire, DurataIncetinireRamasa);
            DurataIncetinireRamasa-=Time.deltaTime;
            yield return new WaitForSeconds(1f);
        }
    }



}
