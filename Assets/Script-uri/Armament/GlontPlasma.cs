using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlontPlasma : Glont
{

    public int Strapungere;

    public bool Infometat = false;
    public float MultiplicatorInfometat;

    public int NumarPetale;
    public GlontPlasma Petala;

    

    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
        
    }

    // Update is called once per frame
    public override void Update()
    {
        base.Update();
    }

    public override void FixedUpdate()
    {
        base.FixedUpdate();
        if(Infometat)
        {
            Daune += distantaParcursa * MultiplicatorInfometat;
        }
        
    }

    public override void Deplaseaza()
    {
        corp.velocity = transform.up * VitezaGlontului * Time.deltaTime;
    }

    

    public override void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Inamic"))
        {
            var inamic = collision.GetComponent<InamicBaza>();
            var contact = colliderGlont.ClosestPoint(inamic.gameObject.transform.position);
            if (Infometat)
            {
                
                inamic.EsteDistrus += ActiveazaBonusInfometat;
            }
            else if (NumarPetale > 0)
            {
                
                inamic.EsteDistrus += ActiveazaBonusPetale;
                
                
            }

            inamic.Ranit(Daune, contact);

            inamic.EsteDistrus -= ActiveazaBonusPetale;
            inamic.EsteDistrus -= ActiveazaBonusInfometat;

            if (Strapungere == 0)
            {
                
                Destroy(gameObject);
            }
            else
            {
                
                Daune /= 2;
                Strapungere--;
            }
            
        }
    }

    public void ActiveazaBonusInfometat()
    {
        VitezaGlontului *= 2f;
        Distanta *= 2f;
    }

    public void ActiveazaBonusPetale()
    {

        var distantaDintreGloante = 360 / NumarPetale;
        var decalaj = (360 / 2) - (distantaDintreGloante / 2);
        for (int i = 0; i < NumarPetale; i++)
        {
            var unghiRotatie = distantaDintreGloante * i;
            var rotatieCalculata = Quaternion.Euler(new Vector3(0, 0, transform.rotation.eulerAngles.z + unghiRotatie - decalaj));
            var glontNou = Instantiate(Petala, transform.position, rotatieCalculata);
            glontNou.Daune = Daune / 2f;
            glontNou.Distanta = Distanta;
            glontNou.VitezaGlontului = VitezaGlontului;
            glontNou.NumarPetale = NumarPetale;
            glontNou.Petala = Petala;
        }

    }

}
