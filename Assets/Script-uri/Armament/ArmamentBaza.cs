using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmamentBaza : MonoBehaviour
{

    public ArmamentScriptableObject dateArmament;
    

    public float PutereDauneCurenta;
    public float DistantaGlontCurenta;
    public float VitezaGlontCurenta;
    public int NumarProiectileCurent;

    public float bonusCombo = 0f;

    public bool PoateAtaca = true;

    public int Nivel;

    [Range(0,360)]
    public float unghiDeImprastiere;



    public virtual void Ataca(Transform pozitieStartGlont, Vector2 directie, float bonusCombo)
    {
        
        this.bonusCombo = bonusCombo;
        var distantaDintreGloante = unghiDeImprastiere / NumarProiectileCurent;
        var decalaj = (unghiDeImprastiere / 2) - (distantaDintreGloante / 2);
        for (int i = 0; i < NumarProiectileCurent; i++)
        {
            var unghiRotatie = distantaDintreGloante * i;
            var rotatieCalculata = Quaternion.Euler(new Vector3(0, 0, pozitieStartGlont.rotation.eulerAngles.z + unghiRotatie - decalaj));

            var glontInstantiat = Instantiate(dateArmament.GlontSpecific, pozitieStartGlont.position, rotatieCalculata);
            SeteazaCaracteristiciGlont(glontInstantiat);
        }


    }

    public virtual void SeteazaCaracteristiciGlont(Glont glontInstantiat)
    {
        glontInstantiat.Daune = PutereDauneCurenta + bonusCombo;
        glontInstantiat.Distanta = DistantaGlontCurenta;
        glontInstantiat.VitezaGlontului = VitezaGlontCurenta;
    }

    protected virtual void Awake()
    {
        PutereDauneCurenta = dateArmament.Putere;
        DistantaGlontCurenta = dateArmament.Distanta;
        VitezaGlontCurenta = dateArmament.Viteza;
        NumarProiectileCurent = dateArmament.NumarGloante;
    }

    public virtual void TransferDate(ArmamentBaza altArmament)
    {
        altArmament.PutereDauneCurenta = PutereDauneCurenta;
        altArmament.Nivel = Nivel;
        altArmament.NumarProiectileCurent = NumarProiectileCurent;
        altArmament.DistantaGlontCurenta = DistantaGlontCurenta;
        altArmament.bonusCombo = bonusCombo;
        altArmament.dateArmament = dateArmament;
        altArmament.VitezaGlontCurenta = VitezaGlontCurenta;
        altArmament.unghiDeImprastiere = unghiDeImprastiere;
        
    }

    protected virtual void Start()
    {
        
    }

    protected virtual void Update()
    {
        
    }

    protected virtual void FixedUpdate()
    {

    }

    public virtual void Accepta(IVisitor visitor)
    {
        visitor.Visit(this);
    }
}
