using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmamentRacheteIzbucnire : ArmamentAruncatorRachete
{

    [SerializeField]
    private float timpIntreLansareGloante;

    public override void Ataca(Transform pozitieStartGlont, Vector2 directie, float bonusCombo)
    {
        StartCoroutine(AtacIzbucnire(pozitieStartGlont, directie, bonusCombo));
    }

    public override void SeteazaCaracteristiciGlont(Glont glontInstantiat)
    {
        base.SeteazaCaracteristiciGlont(glontInstantiat);
        ((GlontRacheta)glontInstantiat).DauneExplozie = DauneExplozie;
        ((GlontRacheta)glontInstantiat).RazaExplozie = RazaExplozie;

    }

    public IEnumerator AtacIzbucnire(Transform pozitieStartGlont, Vector2 directie, float bonusCombo)
    {
        this.bonusCombo = bonusCombo;
        for (int i = 0; i< NumarProiectileCurent; i++)
        {
            var glontInstantiat = Instantiate(dateArmament.GlontSpecific, pozitieStartGlont.position, pozitieStartGlont.rotation);
            SeteazaCaracteristiciGlont(glontInstantiat);
            yield return new WaitForSeconds(timpIntreLansareGloante);
        }
    }

    protected override void Awake()
    {
        
    }

    protected override void Start()
    {
        base.Start();
    }

    protected override void FixedUpdate()
    {
        base.FixedUpdate();
    }

    protected override void Update()
    {
        base.Update();
    }
}
