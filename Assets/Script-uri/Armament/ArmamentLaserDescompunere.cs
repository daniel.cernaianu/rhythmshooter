using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmamentLaserDescompunere : ArmamentRazaLaser
{

    [SerializeField]
    private GlontLaser Fascicul;
    [SerializeField]
    private int NumarFascicule;
    [SerializeField]
    private float TicMaxim;

    public override void Ataca(Transform pozitieStartGlont, Vector2 directie, float bonusCombo)
    {

        base.Ataca(pozitieStartGlont, directie, bonusCombo);
    }


    public override void SeteazaCaracteristiciLaser(Glont glontInstantiat)
    {
        base.SeteazaCaracteristiciLaser(glontInstantiat);
        ((GlontLaser)glontInstantiat).TicMaxim = TicMaxim;
        ((GlontLaser)glontInstantiat).Fascicul = Fascicul;
        ((GlontLaser)glontInstantiat).NumarFascicule = NumarFascicule;

    }

    protected override void Awake()
    {

    }

    protected override void Start()
    {

        base.Start();
    }


    protected override void Update()
    {
        base.Update();
    }

    protected override void FixedUpdate()
    {
        base.FixedUpdate();
    }
}
