using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmamentLaserSoare : ArmamentRazaLaser
{
    [SerializeField]
    private bool Soare;



    public override void Ataca(Transform pozitieStartGlont, Vector2 directie, float bonusCombo)
    {

        this.bonusCombo = bonusCombo;
        if (EtapaCurentaIncarcare == EtapeIncarcare)
        {
            var distantaDintreGloante = 360 / NumarProiectileCurent;
            var decalaj = (360 / 2) - (distantaDintreGloante / 2);
            for (int i = 0; i < NumarProiectileCurent; i++)
            {
                var unghiRotatie = distantaDintreGloante * i;
                var rotatieCalculata = Quaternion.Euler(new Vector3(0, 0, pozitieStartGlont.rotation.eulerAngles.z + unghiRotatie - decalaj));

                var glontInstantiat = Instantiate(dateArmament.GlontSpecific, pozitieStartGlont.position, rotatieCalculata, pozitieStartGlont);
                SeteazaCaracteristiciLaser(glontInstantiat);

            }

            StartCoroutine(AsteaptaLaser());
        }
        else 
        { 
            EtapaCurentaIncarcare++;
            InvoacaEvenimentActualizare();
        }

    }


    public override void SeteazaCaracteristiciLaser(Glont glontInstantiat)
    {
        base.SeteazaCaracteristiciLaser(glontInstantiat);
        ((GlontLaser)glontInstantiat).Soare = Soare;

    }

    protected override void Awake()
    {
        
    }

    protected override void Start()
    {

        base.Start();
    }


    protected override void Update()
    {
        base.Update();
    }

    protected override void FixedUpdate()
    {
        base.FixedUpdate();
    }
}
