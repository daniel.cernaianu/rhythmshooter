using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Glont : MonoBehaviour
{

    public float VitezaGlontului;
    public float Daune;
    public float Distanta;
    public float distantaParcursa;

    protected Collider2D colliderGlont;

    public Rigidbody2D corp;


    // Start is called before the first frame update
    public virtual void Start()
    {
        distantaParcursa = 0f;
        colliderGlont = GetComponent<Collider2D>();
        corp = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    public virtual void Update()
    {
        
    }

    public virtual void FixedUpdate()
    {
        Deplaseaza();
        
        distantaParcursa += corp.velocity.magnitude * Time.deltaTime;
        if ( distantaParcursa >= Distanta)
            Destroy(gameObject);
    }

    public virtual void Deplaseaza()
    {
        
    }

    public virtual void Awake()
    {

    }

    public virtual void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.CompareTag("Inamic"))
        {

            var inamic = collider.GetComponent<InamicBaza>();

            var contact = colliderGlont.ClosestPoint(inamic.gameObject.transform.position);

            inamic.Ranit(Daune, contact);
            
            Destroy(gameObject);
        }
    }

}
