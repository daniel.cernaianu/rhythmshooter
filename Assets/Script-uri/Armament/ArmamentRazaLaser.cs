using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmamentRazaLaser : ArmamentBaza
{

    public int EtapeIncarcare;
    public float RezistentaLaser;
    public int EtapaCurentaIncarcare = 0;
    public event Action ActualizeazaUIEtape;
    public event Action ReseteazaEtape;
    

    public override void Ataca(Transform pozitieStartGlont, Vector2 directie, float bonusCombo)
    {
        this.bonusCombo = bonusCombo;
        if (EtapaCurentaIncarcare == EtapeIncarcare)
        {
            var distantaDintreGloante = unghiDeImprastiere / NumarProiectileCurent;
            var decalaj = (unghiDeImprastiere / 2) - (distantaDintreGloante / 2);
            for (int i = 0; i < NumarProiectileCurent; i++)
            {
                var unghiRotatie = distantaDintreGloante * i;
                var rotatieCalculata = Quaternion.Euler(new Vector3(0, 0, pozitieStartGlont.rotation.eulerAngles.z + unghiRotatie - decalaj));

                var glontInstantiat = Instantiate(dateArmament.GlontSpecific, pozitieStartGlont.position, rotatieCalculata, pozitieStartGlont);
                SeteazaCaracteristiciLaser(glontInstantiat);

            }

            StartCoroutine(AsteaptaLaser());
        }
        else
        {
            EtapaCurentaIncarcare++;
            InvoacaEvenimentActualizare();
        }
    }

    public IEnumerator AsteaptaLaser()
    {
        PoateAtaca = false;
        yield return new WaitForSeconds(RezistentaLaser);
        PoateAtaca = true;
        EtapaCurentaIncarcare = 0;
        ReseteazaEtape.Invoke();
    }

    protected void InvoacaEvenimentActualizare()
    {
        Debug.Log(ActualizeazaUIEtape);
        ActualizeazaUIEtape.Invoke();
    }

    public virtual void SeteazaCaracteristiciLaser(Glont glontInstantiat)
    {
        glontInstantiat.Daune = PutereDauneCurenta + VitezaGlontCurenta + bonusCombo;
        ((GlontLaser)glontInstantiat).Rezistenta = RezistentaLaser + DistantaGlontCurenta;
    }

    public override void TransferDate(ArmamentBaza altArmament)
    {
        base.TransferDate(altArmament);
        ((ArmamentRazaLaser)altArmament).RezistentaLaser = RezistentaLaser;
        ((ArmamentRazaLaser)altArmament).EtapeIncarcare = EtapeIncarcare;

    }

    protected override void Awake()
    {
        base.Awake();
       
    }

    protected override void Start()
    {
        
        base.Start();
    }


    protected override void Update()
    {
        base.Update();
    }

    protected override void FixedUpdate()
    {
        base.FixedUpdate();
    }

    public override void Accepta(IVisitor visitor)
    {
        base.Accepta(visitor);
        visitor.Visit(this);
    }

}
