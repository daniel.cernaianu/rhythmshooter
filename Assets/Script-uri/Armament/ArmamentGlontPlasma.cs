using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmamentGlontPlasma : ArmamentBaza
{

    
    public int Strapungere;

    public override void Ataca(Transform pozitieStartGlont, Vector2 directie, float bonusCombo)
    {
        base.Ataca(pozitieStartGlont, directie, bonusCombo);
    }

    public override void SeteazaCaracteristiciGlont(Glont glontInstantiat)
    {
        base.SeteazaCaracteristiciGlont(glontInstantiat);
        ((GlontPlasma)glontInstantiat).Strapungere = Strapungere;
        
    }

    public override void TransferDate(ArmamentBaza altArmament)
    {
        base.TransferDate(altArmament);
        ((ArmamentGlontPlasma)altArmament).Strapungere = Strapungere;

    }

    protected override void Awake()
    {
        base.Awake();
    }

    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
        
    }

    // Update is called once per frame
    protected override void FixedUpdate()
    {
        base.FixedUpdate();
    }

    public override void Accepta(IVisitor visitor)
    {
        base.Accepta(visitor);
        visitor.Visit(this);
    }

}
