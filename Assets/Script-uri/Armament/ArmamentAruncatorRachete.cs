using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmamentAruncatorRachete : ArmamentBaza
{

    public float RazaExplozie;
    public float DauneExplozie;
    

    public override void Ataca(Transform pozitieStartGlont, Vector2 directie, float bonusCombo)
    {
        base.Ataca(pozitieStartGlont, directie, bonusCombo);
    }

    public override void SeteazaCaracteristiciGlont(Glont glontInstantiat)
    {
        base.SeteazaCaracteristiciGlont(glontInstantiat);
        ((GlontRacheta)glontInstantiat).DauneExplozie = DauneExplozie;
        ((GlontRacheta)glontInstantiat).RazaExplozie = RazaExplozie;
        
    }

    protected override void Awake()
    {
        base.Awake();
        RazaExplozie = 1f;
        DauneExplozie = PutereDauneCurenta / 2f;
    }

    public override void TransferDate(ArmamentBaza altArmament)
    {
        base.TransferDate(altArmament);
        ((ArmamentAruncatorRachete)altArmament).RazaExplozie = RazaExplozie;
        ((ArmamentAruncatorRachete)altArmament).DauneExplozie = DauneExplozie;
        
    }

    protected override void Start()
    {
        base.Start();
    }

    protected override void FixedUpdate()
    {
        base.FixedUpdate();
    }

    protected override void Update()
    {
        base.Update();
    }

    public override void Accepta(IVisitor visitor)
    {
        base.Accepta(visitor);
        visitor.Visit(this);
    }

}
