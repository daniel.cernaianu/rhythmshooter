using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmamentRacheteHaos : ArmamentAruncatorRachete
{
    [SerializeField]
    private int NumarExplozii;
    private List<Vector2> PozitiiExplozii = new List<Vector2>();


    public override void Ataca(Transform pozitieStartGlont, Vector2 directie, float bonusCombo)
    {
        base.Ataca(pozitieStartGlont, directie, bonusCombo);
    }

    public override void SeteazaCaracteristiciGlont(Glont glontInstantiat)
    {
        base.SeteazaCaracteristiciGlont(glontInstantiat);
        ((GlontRacheta)glontInstantiat).DauneExplozie = DauneExplozie;
        ((GlontRacheta)glontInstantiat).RazaExplozie = RazaExplozie;
        ((GlontRacheta)glontInstantiat).NumarExplozii = NumarExplozii;
        Debug.Log(PozitiiExplozii);
        PozitiiExplozii.Add(Vector2.zero);

        

        for(int i = 1; i < NumarExplozii; i++)
        {
            var pozitieX = Random.Range(-2f, 2f);
            var pozitieY = Random.Range(-2f, 2f);

            PozitiiExplozii.Add(new Vector2(pozitieX, pozitieY));
        }

        ((GlontRacheta)glontInstantiat).PozitiiExplozii = PozitiiExplozii;
    }

    

    protected override void Awake()
    {

    }

    protected override void Start()
    {
        base.Start();
    }

    protected override void FixedUpdate()
    {
        base.FixedUpdate();
    }

    protected override void Update()
    {
        base.Update();
    }
}
