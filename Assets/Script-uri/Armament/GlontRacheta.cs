using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlontRacheta : Glont
{

    public float RazaExplozie;
    public float DauneExplozie;
    public List<Vector2> PozitiiExplozii;
    public int NumarExplozii;

    public override void Awake()
    {
        base.Awake();
        
        
    }

    public override void Deplaseaza()
    {
        corp.AddForce(transform.up * VitezaGlontului, ForceMode2D.Impulse);
    }

    public override void FixedUpdate()
    {
        base.FixedUpdate();
    }

    public override void OnTriggerEnter2D(Collider2D collider)
    {
        base.OnTriggerEnter2D(collider);
    }

    public override void Start()
    {
        base.Start();
    }

    public override void Update()
    {
        base.Update();
    }

    public void OnDestroy()
    {
        Explodeaza();
    }

    public void Explodeaza()
    {
        
        for (int i = 0; i < NumarExplozii; i++)
        {
            var colliders = Physics2D.OverlapCircleAll((Vector2)transform.position + PozitiiExplozii[i], RazaExplozie);
            CatalogVFX.instanta.RuleazaVFX("explozie_racheta", (Vector2)transform.position + PozitiiExplozii[i], Quaternion.identity, RazaExplozie);
            foreach (Collider2D col in colliders)
            {
                if (col.CompareTag("Inamic"))
                {
                    var inamic = col.GetComponent<InamicBaza>();
                    var contact = col.ClosestPoint((Vector2)transform.position + PozitiiExplozii[i]);

                    inamic.Ranit(DauneExplozie, contact);
                }
            }
        }
    }
}
