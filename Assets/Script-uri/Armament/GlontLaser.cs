using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlontLaser : Glont
{

    private const float _unghi360 = 360f;


    public float Rezistenta;
    public int NumarFascicule;
    public GlontLaser Fascicul;
    public float TicMaxim;
    public LineRenderer laser;
    public bool Soare;

    private float tic = 0f;


    public override void Deplaseaza()
    {
        
    }

    public override void FixedUpdate()
    {
        
    }

    public override void OnTriggerEnter2D(Collider2D collider)
    {
        
    }

    public void OnTriggerStay2D(Collider2D collider)
    {
        
        if (collider.gameObject.CompareTag("Inamic"))
        {
            var inamic = collider.GetComponent<InamicBaza>();
            var contact = colliderGlont.ClosestPoint(inamic.gameObject.transform.position);

            if (TicMaxim > 0)
            {
                tic += Time.deltaTime;
                if (tic >= TicMaxim)
                {
                    GenereazaFasciculeDescompunere(collider.transform);
                    tic = 0f;
                }
            }

            inamic.Ranit(Daune, contact);
            
            
        }
    }

    public void GenereazaFasciculeDescompunere(Transform pozitie)
    {
        var distantaDintreGloante = _unghi360 / NumarFascicule;
        var decalaj = (_unghi360 / 2) - (distantaDintreGloante / 2);
        for (int i = 0; i < NumarFascicule; i++)
        {
            var unghiRotatie = distantaDintreGloante * i;
            var rotatieCalculata = Quaternion.Euler(new Vector3(0, 0, pozitie.rotation.eulerAngles.z + unghiRotatie - decalaj));

            var glontInstantiat = Instantiate(Fascicul, pozitie.position, rotatieCalculata);
            glontInstantiat.Daune = Daune/2f;
            ((GlontLaser)glontInstantiat).Rezistenta = Rezistenta/2f;
            ((GlontLaser)glontInstantiat).Fascicul = Fascicul;
            ((GlontLaser)glontInstantiat).NumarFascicule = NumarFascicule;
            


        }
    }

    public void IncepeInitializare()
    {
        
        var collider = GetComponent<BoxCollider2D>();
        collider.enabled = false;
    }

    public void FinalizeazaInitializarea()
    {
        var collider = GetComponent<BoxCollider2D>();
        collider.enabled = true;
        
    }

    public void OnDestroy()
    {
        
    }

    public override void Start()
    {
        tic = 0;
        laser = GetComponent<LineRenderer>();
        
        colliderGlont = GetComponent<Collider2D>();
        if(Soare)
        {
            laser.SetPosition(1, new Vector3(0, 30, 0));
            ((BoxCollider2D)colliderGlont).size = new Vector2(0.3f, 30f);
            colliderGlont.offset = new Vector2(0f, 15f);
        }
        
        Destroy(gameObject, Rezistenta);
    }

    public override void Update()
    {
        //base.Update();
    }

 
}
