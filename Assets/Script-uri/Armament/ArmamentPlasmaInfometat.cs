using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmamentPlasmaInfometat : ArmamentGlontPlasma
{
    [SerializeField]
    private bool Infometat;
    [SerializeField]
    private float MultiplicatorInfometat;

    public override void Ataca(Transform pozitieStartGlont, Vector2 directie, float bonusCombo)
    {

        this.bonusCombo = bonusCombo;
        var glontInstantiat = Instantiate(dateArmament.GlontSpecific, pozitieStartGlont.position, pozitieStartGlont.rotation);
        SeteazaCaracteristiciGlont(glontInstantiat);
        
    }

    public override void SeteazaCaracteristiciGlont(Glont glontInstantiat)
    {
        glontInstantiat.Daune = PutereDauneCurenta;
        glontInstantiat.Distanta = DistantaGlontCurenta;
        glontInstantiat.VitezaGlontului = VitezaGlontCurenta + NumarProiectileCurent;
        ((GlontPlasma)glontInstantiat).Strapungere = Strapungere;
        ((GlontPlasma)glontInstantiat).Infometat = Infometat;
        ((GlontPlasma)glontInstantiat).MultiplicatorInfometat = MultiplicatorInfometat;

    }

    public override void TransferDate(ArmamentBaza altArmament)
    {
        base.TransferDate(altArmament);
        ((ArmamentGlontPlasma)altArmament).Strapungere = Strapungere;

    }

    protected override void Awake()
    {

    }

    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();

    }

    // Update is called once per frame
    protected override void FixedUpdate()
    {
        base.FixedUpdate();
    }
}
